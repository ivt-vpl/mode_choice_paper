TARGET_DIRECTORY=/run/media/sebastian/shoerl_data/mode_choice_paper
mkdir -p $TARGET_DIRECTORY/plots
i=1

for version in constrained unconstrained; do
    for model in trip tour plan advanced; do
        echo "${model}_${version}"
        mkdir -p $TARGET_DIRECTORY/plots/${model}_${version}
        (python3 extract_data.py $TARGET_DIRECTORY/trips_from_mz15.csv $TARGET_DIRECTORY/${model}_${version}_trips.csv $TARGET_DIRECTORY/plots/${model}_${version}) &

        if [ $(($i % 4)) -eq 0 ]; then
            wait
        fi

        i=$(($i + 1))
    done
done
