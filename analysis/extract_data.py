import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import palettable, pickle
from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import FixedLocator
import sys

mz_input = sys.argv[1]
simulation_input = sys.argv[2]
output_path = sys.argv[3]

# Input
use_weights = True

# Output
output_type = "pdf"
dpi = 300

# Analysis area
center = np.array([2683253.0, 1246745.0]) # Bellevue
radius = 30000.0 - 1000.0

# Modes
modes = ["car", "pt", "bike", "walk"]
mode_labels = ["Private Car", "Public Transport", "Cycling", "Walking"]
mode_colors = palettable.tableau.Tableau_10.mpl_colors

# Choose network_distance or crowfly_distance for all plots
distance = "network_distance"

# Settings for "mode choice by distance" plot
maximum_distance = 20 # km
bins = 20

use_quantiles = True # Quantile-based bins for the mode share by distance plot
quantiles_basis = "mz" # Use "mz" or "simulation" to compute the quantile-based bins boundaries

# Settings for travel time comparison
maximum_travel_times = { # seconds
    "car" : 3600 * 1.5,
    "pt" : 3600 * 1.0,
    "bike" : 3600 * 0.5,
    "walk" : 3600 * 0.25
}

# Settings for travel distance comparison
maximum_travel_distances = { # km
    "car" : 20,
    "pt" : 20,
    "bike" : 10,
    "walk" : 5,
}

# Settings for travel speed comparison
maximum_travel_speeds = { # km / h
    "car" : 100,
    "pt" : 60,
    "bike" : 20,
    "walk" : 10,
}

output_data = {}

# Load data

df_mz = pd.read_csv(mz_input, delimiter = ";")
df_simulation = pd.read_csv(simulation_input, delimiter = ";")

if not use_weights:
    df_mz.loc[:, "weight"] = 1

outside_selector  = (df_simulation["preceedingPurpose"] == "outside")
outside_selector |= (df_simulation["followingPurpose"] == "outside")
outside_selector |= (df_simulation["mode"] == "outside")

df_simulation = df_simulation[~outside_selector]

# Filter data to analysis area

df_mz = df_mz[np.sqrt((df_mz["origin_x"] - center[0])**2 + (df_mz["origin_y"] - center[1])**2) < radius]
df_simulation = df_simulation[np.sqrt((df_simulation["origin_x"] - center[0])**2 + (df_simulation["origin_y"] - center[1])**2) < radius]

df_mz = df_mz[np.sqrt((df_mz["destination_x"] - center[0])**2 + (df_mz["destination_y"] - center[1])**2) < radius]
df_simulation = df_simulation[np.sqrt((df_simulation["destination_x"] - center[0])**2 + (df_simulation["destination_y"] - center[1])**2) < radius]

df_basis = df_mz if quantiles_basis == "mz" else df_simulation

# Compute bin boundaries for mode-choice-by-distance plot

bounds = np.linspace(0, maximum_distance, bins + 1)

if use_quantiles:
    max_quantile = (np.sum(np.sort(df_basis[distance]) < maximum_distance) + 1) / len(df_basis[distance])
    bounds = np.array([np.percentile(df_basis[distance], q * 100) for q in np.linspace(0, max_quantile, bins + 1)])

output_data["bounds_by_distance"] = bounds

# Compute distance bins for all trips

df_mz.loc[:, "distance_class"] = np.digitize(df_mz[distance], bins = bounds[1:], right = True)
df_simulation.loc[:, "distance_class"] = np.digitize(df_simulation[distance], bins = bounds[1:], right = True)

# Compute total trips (independent of mode) in each bin

totals_mz = np.array([ np.sum(df_mz[df_mz["distance_class"] == b]["weight"]) for b in range(bins + 1)])
totals_simulation = np.array([ np.sum(df_simulation["distance_class"] == b) for b in range(bins + 1)])

output_data["totals_mz_by_distance"] = totals_mz
output_data["totals_simulation_by_distance"] = totals_simulation

# Number of observations per distance

plt.figure(figsize = (7, 4), dpi = dpi)

plt.bar(np.arange(bins), totals_mz[:-1] / np.sum(totals_mz[:-1]), width = 0.3, label = "Reference", color = "k", linewidth = 0)
plt.bar(np.arange(bins) + 0.3, totals_simulation[:-1] / np.sum(totals_simulation[:-1]), 0.3, label = "Simulation", color = "gray", linewidth = 0)

plt.gca().xaxis.set_major_locator(FixedLocator(np.arange(bins) + 0.3))
plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: ("< %.2fkm" % bounds[int(x - 0.3) + 1]) if x > 0 else ""))

labels = plt.gca().get_xticklabels()
plt.setp(labels, rotation=30, fontsize=8)
plt.grid()
plt.legend(loc = "best")
plt.xlabel("Distance Bins")
plt.ylabel("Trip Density")

plt.savefig("%s/output_trip_density_by_distance.%s" % (output_path, output_type))

# Mode-share-by-distance plot

plt.figure(figsize = (7, 4), dpi = dpi)
output_data["mode_share_by_distance"] = {}

handles = []
data = []

for mode, mode_label, color in zip(modes, mode_labels, mode_colors):
    counts_mz = np.array([
        np.sum(df_mz[(df_mz["distance_class"] == b) & (df_mz["mode"] == mode)]["weight"]) for b in range(bins + 1)
    ])

    counts_simulation = np.array([
        np.sum((df_simulation["distance_class"] == b) & (df_simulation["mode"] == mode)) for b in range(bins + 1)
    ])

    plt.plot(bounds[1:], counts_mz[:-1] / totals_mz[:-1], color = color,linestyle = '--')
    handle, = plt.plot(bounds[1:], counts_simulation[:-1] / totals_simulation[:-1], label = mode_label, color = color)
    handles.append(handle)

    output_data["mode_share_by_distance"][mode] = dict(
        mz = counts_mz,
        simulation = counts_simulation
    )

for bound in bounds[1:]:
    plt.plot([bound, bound], [0.0, 1.0], 'k', linewidth = 0.5, alpha = 0.2)

for i in range(10):
    plt.plot([0, bounds[-1]], [i * 0.1, i * 0.1], 'k', linewidth = 0.5, alpha = 0.2)

#plt.grid()

plt.ylabel("Mode Share")
plt.xlabel("Travel Distance")
plt.xlim([bounds[1], bounds[-1]])

#plt.legend(loc = "upper right", ncol = 2)
plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: ("%.2f km" % x) if x > 0 else ""))
plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, p: ("%.0f%%" % (100.0 * x)) if x > 0 else ""))

solid_line, = plt.plot([0.0, 0.0], [0.0, 0.0], 'k-', label = "Simulation")
dashed_line, = plt.plot([0.0, 0.0], [0.0, 0.0], 'k--', label = "Reference")
plt.legend(handles = handles + [solid_line, dashed_line], ncol = 3)

plt.savefig("%s/output_mode_share_by_distance.%s" % (output_path, output_type))

# Mode share by number of trips plot

plt.figure(figsize = (7, 4), dpi = dpi)

by_mode_mz = [np.sum(df_mz[df_mz["mode"] == mode]["weight"]) / np.sum(df_mz["weight"]) for mode in modes]
by_mode_simulation = [np.sum(df_simulation["mode"] == mode) / len(df_simulation) for mode in modes]

plt.bar(np.arange(len(modes)), by_mode_mz, width = 0.3, color = "gray", linewidth = 0, label = "Reference")
bar = plt.bar(np.arange(len(modes)) + 0.3, by_mode_simulation, width = 0.3, color = "gray", linewidth = 0, label = "Simulation")

for i in range(len(bar)):
    bar[i].set_color(mode_colors[i])

plt.grid()
plt.legend(loc = "best")

plt.gca().xaxis.set_major_locator(FixedLocator(np.arange(len(modes)) + 0.3))
plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: mode_labels[int(x)]))
plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, p: ("%.0f%%" % (100.0 * x)) if x > 0 else ""))

plt.ylabel("Mode Share")

plt.savefig("%s/output_mode_share.%s" % (output_path, output_type))

output_data["modes"] = modes
output_data["counts_by_mode_mz"] = by_mode_mz
output_data["counts_by_mode_simulation"] = by_mode_simulation

# Travel time comparison

plt.figure(figsize = (10, 6), dpi = dpi)
output_data["travel_time"] = {}

for i, (mode, color, mode_label) in enumerate(zip(modes, mode_colors, mode_labels)):
    plt.subplot(2, 2, i + 1)
    maximum_travel_time = maximum_travel_times[mode]

    selector_mz = (df_mz["mode"] == mode) & (df_mz["travel_time"] <= maximum_travel_time)
    selector_simulation = (df_simulation["mode"] == mode) & (df_simulation["travel_time"] <= maximum_travel_time)
    weights = [df_mz[selector_mz]["weight"].values, np.ones((int(np.sum(selector_simulation)),))]

    plt.hist([
        df_mz[selector_mz]["travel_time"].values,
        df_simulation[selector_simulation]["travel_time"].values
    ], normed = True, bins = bins, color = ["gray", color], linewidth = 0, label = ["Reference", "Simulation"], weights = weights)

    plt.grid()
    plt.legend()
    plt.xlabel("Travel Time [min]")

    plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, p: ""))
    plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: "%d" % (x / 60) if x > 0 else ""))
    plt.xlim([0, maximum_travel_time])

    plt.title(mode_label)

    output_data["travel_time"][mode] = dict(
        mz = df_mz[selector_mz]["travel_time"].values,
        simulation = df_simulation[selector_simulation]["travel_time"].values
    )

plt.tight_layout()

plt.savefig("%s/output_travel_times.%s" % (output_path, output_type))

# Travel distance comparison

plt.figure(figsize = (10, 6), dpi = dpi)
output_data["travel_distance"] = {}

for i, (mode, color, mode_label) in enumerate(zip(modes, mode_colors, mode_labels)):
    plt.subplot(2, 2, i + 1)
    maximum_travel_distance = maximum_travel_distances[mode]

    selector_mz = (df_mz["mode"] == mode) & (df_mz[distance] <= maximum_travel_distance)
    selector_simulation = (df_simulation["mode"] == mode)& (df_simulation[distance] <= maximum_travel_distance)
    weights = [df_mz[selector_mz]["weight"].values, np.ones((int(np.sum(selector_simulation)),))]

    plt.hist([
        df_mz[selector_mz][distance].values,
        df_simulation[selector_simulation][distance].values
    ], normed = True, bins = bins, color = ["gray", color], linewidth = 0, label = ["Reference", "Simulation"], weights = weights)

    plt.grid()
    plt.legend()
    plt.xlabel("Travel Distance [km]")

    plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, p: ""))
    plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: "%d" % x if x > 0 else ""))
    plt.xlim([0, maximum_travel_distance])

    plt.title(mode_label)

    output_data["travel_distance"][mode] = dict(
        mz = df_mz[selector_mz][distance].values,
        simulation = df_simulation[selector_simulation][distance].values
    )

plt.tight_layout()

plt.savefig("%s/output_distances.%s" % (output_path, output_type))

# Travel speed comparison

plt.figure(figsize = (10, 6), dpi = dpi)
output_data["travel_speed"] = {}

df_mz["travel_speed"] = df_mz[distance] / df_mz["travel_time"] * 3600
df_simulation["travel_speed"] = df_simulation[distance] / df_simulation["travel_time"] * 3600

for i, (mode, color, mode_label) in enumerate(zip(modes, mode_colors, mode_labels)):
    plt.subplot(2, 2, i + 1)
    maximum_travel_speed = maximum_travel_speeds[mode]

    selector_mz = (df_mz["mode"] == mode) & (df_mz["travel_speed"] <= maximum_travel_speed)
    selector_simulation = (df_simulation["mode"] == mode) & (df_simulation["travel_speed"] <= maximum_travel_speed)
    weights = [df_mz[selector_mz]["weight"].values, np.ones((int(np.sum(selector_simulation)),))]

    plt.hist([
        df_mz[selector_mz]["travel_speed"].values,
        df_simulation[selector_simulation]["travel_speed"].values
    ], normed = True, bins = bins, color = ["gray", color], linewidth = 0, label = ["Reference", "Simulation"], weights = weights)

    plt.grid()
    plt.legend()
    plt.xlabel("Travel Speed [km/h]")

    plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, p: ""))
    plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: "%d" % x if x > 0 else ""))
    plt.xlim([0, maximum_travel_speed])

    plt.title(mode_label)

    output_data["travel_speed"][mode] = dict(
        mz = df_mz[selector_mz]["travel_speed"].values,
        simulation = df_simulation[selector_simulation]["travel_speed"].values
    )

plt.tight_layout()

plt.savefig("%s/output_speeds.%s" % (output_path, output_type))
# Mode-share-by-time of day plot

daytime_bins = 18
daytime_bounds = np.arange(4, 4 + daytime_bins + 1) * 3600.0

output_data["bounds_daytime"] = daytime_bounds
output_data["mode_share_by_daytime"] = {}

df_mz.loc[:, "daytime_class"] = np.digitize(df_mz["start_time"], bins = daytime_bounds[1:], right = True)
df_simulation.loc[:, "daytime_class"] = np.digitize(df_simulation["start_time"], bins = daytime_bounds[1:], right = True)

totals_mz = np.array([ np.sum(df_mz[df_mz["daytime_class"] == b]["weight"]) for b in range(daytime_bins + 1)])
totals_simulation = np.array([ np.sum(df_simulation["daytime_class"] == b) for b in range(daytime_bins + 1)])

output_data["totals_by_daytime"] = dict(
    mz = totals_mz,
    simulation = totals_simulation
)

plt.figure(figsize = (7, 4), dpi = dpi)

for mode, color in zip(modes, mode_colors):
    counts_mz = np.array([
        np.sum(df_mz[(df_mz["daytime_class"] == b) & (df_mz["mode"] == mode)]["weight"]) for b in range(daytime_bins + 1)
    ])

    counts_simulation = np.array([
        np.sum((df_simulation["daytime_class"] == b) & (df_simulation["mode"] == mode)) for b in range(daytime_bins + 1)
    ])

    plt.plot(daytime_bounds[1:], counts_mz[:-1] / totals_mz[:-1], color = color, label = mode, linestyle = '--')
    plt.plot(daytime_bounds[1:], counts_simulation[:-1] / totals_simulation[:-1], color = color)

    output_data["mode_share_by_daytime"][mode] = dict(
        mz = counts_mz,
        simulation = counts_simulation
    )

for bound in daytime_bounds[1:]:
    plt.plot([bound, bound], [0.0, 1.0], 'k', linewidth = 0.5, alpha = 0.2)

for i in range(10):
    plt.plot([0, daytime_bounds[-1]], [i * 0.1, i * 0.1], 'k', linewidth = 0.5, alpha = 0.2)

plt.ylabel("Mode Share")
plt.xlim([daytime_bounds[1], daytime_bounds[-1]])
plt.ylim([0, 0.7])

solid_line, = plt.plot([0.0, 0.0], [0.0, 0.0], 'k-', label = "Simulation")
dashed_line, = plt.plot([0.0, 0.0], [0.0, 0.0], 'k--', label = "Reference")
plt.legend(handles = handles + [solid_line, dashed_line], ncol = 3)

plt.gca().xaxis.set_major_locator(FixedLocator(np.arange(0, 100) * 3600 * 2))
plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, p: "%02d:%02d" % (x // 3600, (x % 3600) // 60) if x > 0 else ""))
plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, p: ("%.0f%%" % (100.0 * x)) if x > 0 else ""))

plt.savefig("%s/output_mode_share_by_time_of_day.%s" % (output_path, output_type))

with open("%s/output.p" % output_path, "wb+") as f:
    pickle.dump(output_data, f)
