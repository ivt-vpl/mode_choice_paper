TARGET_DIRECTORY=/run/media/sebastian/shoerl_data/mode_choice_paper

mkdir -p $TARGET_DIRECTORY

for version in constrained unconstrained; do
    for model in trip tour plan advanced; do
        scp pikelot:/nas/shoerl/mode_choice_paper/output2_${model}_${version}/output_events.xml.gz $TARGET_DIRECTORY/${model}_${version}_events.xml.gz
        scp pikelot:/nas/shoerl/mode_choice_paper/output2_${model}_${version}/stopwatch.txt $TARGET_DIRECTORY/${model}_${version}_stopwatch.txt
    done
done

mkdir -p $TARGET_DIRECTORY/tracking

for i in $(seq 0 100); do
    scp pikelot:/nas/shoerl/mode_choice_paper/output2_plan_constrained/ITERS/it.${i}/${i}.tracked_travel_times.csv $TARGET_DIRECTORY/tracking
done
