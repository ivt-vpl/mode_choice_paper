TARGET_DIRECTORY=/run/media/sebastian/shoerl_data/mode_choice_paper
i=1

for version in constrained unconstrained; do
    for model in trip tour plan advanced; do
        (java -Xmx10G -cp ../target/mode_choice_paper-0.0.1-SNAPSHOT.jar ch.ethz.matsim.baseline_scenario.analysis.trips.run.ConvertTripsFromEvents $TARGET_DIRECTORY/zurich_network.xml.gz $TARGET_DIRECTORY/${model}_${version}_events.xml.gz $TARGET_DIRECTORY/${model}_${version}_trips.csv) &

        if [ $(($i % 4)) -eq 0 ]; then
            wait
        fi

        i=$(($i + 1))
    done
done
