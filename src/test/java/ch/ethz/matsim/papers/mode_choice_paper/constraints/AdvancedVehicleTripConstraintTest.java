package ch.ethz.matsim.papers.mode_choice_paper.constraints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.population.PopulationUtils;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.router.TripStructureUtils;
import org.matsim.core.router.TripStructureUtils.Trip;

import ch.ethz.matsim.mode_choice.framework.DefaultModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraint;

public class AdvancedVehicleTripConstraintTest {
	@Test
	public void testTwoTripsCase() {
		List<String> availableModes = Arrays.asList("car", "walk");
		List<String> constrainedModes = Arrays.asList("car");
		List<ModeChoiceTrip> trips = createMockTrips("home", "other", "home");

		AdvancedVehicleTripConstraint.Factory factory = new AdvancedVehicleTripConstraint.Factory(constrainedModes);
		TripConstraint constraint = factory.createConstraint(trips, availableModes);

		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "car", "car"));
		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "walk", "walk"));
		Assert.assertEquals(1, getNonFeasibleStage(constraint, trips, "car", "walk"));
		Assert.assertEquals(1, getNonFeasibleStage(constraint, trips, "walk", "car"));
	}

	@Test
	public void testCaseWithoutReturn() {
		List<String> availableModes = Arrays.asList("car", "walk");
		List<String> constrainedModes = Arrays.asList("car");
		List<ModeChoiceTrip> trips = createMockTrips("home", "other1", "other2", "home");

		AdvancedVehicleTripConstraint.Factory factory = new AdvancedVehicleTripConstraint.Factory(constrainedModes);
		TripConstraint constraint = factory.createConstraint(trips, availableModes);

		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "car", "car", "car"));
		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "walk", "walk", "walk"));
		Assert.assertEquals(1, getNonFeasibleStage(constraint, trips, "car", "walk", "walk"));
		Assert.assertEquals(1, getNonFeasibleStage(constraint, trips, "walk", "car", "walk"));
	}

	@Test
	public void testCaseWithReturn() {
		List<String> availableModes = Arrays.asList("car", "walk");
		List<String> constrainedModes = Arrays.asList("car");
		List<ModeChoiceTrip> trips = createMockTrips("home", "other1", "other2", "other1", "home");

		AdvancedVehicleTripConstraint.Factory factory = new AdvancedVehicleTripConstraint.Factory(constrainedModes);
		TripConstraint constraint = factory.createConstraint(trips, availableModes);

		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "car", "car", "car", "car"));
		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "walk", "walk", "walk", "walk"));
		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "car", "walk", "walk", "car"));

		Assert.assertEquals(1, getNonFeasibleStage(constraint, trips, "walk", "car", "car", "car"));
		Assert.assertEquals(2, getNonFeasibleStage(constraint, trips, "car", "walk", "car", "car"));
		Assert.assertEquals(2, getNonFeasibleStage(constraint, trips, "car", "car", "walk", "car"));

		// This is the advanced constraint: The last leg MUST be car, because otherwise
		// the car is not brought back home
		Assert.assertEquals(3, getNonFeasibleStage(constraint, trips, "car", "walk", "walk", "walk"));
	}

	@Test
	public void testCaseFromPopulation() {
		List<String> availableModes = Arrays.asList("car", "walk", "bike", "pt");
		List<String> constrainedModes = Arrays.asList("car", "bike");

		List<ModeChoiceTrip> trips = createMockTrips("home_782403", "other_674862", "other_862430", "other_674862",
				"other_862430", "home_782403");

		AdvancedVehicleTripConstraint.Factory factory = new AdvancedVehicleTripConstraint.Factory(constrainedModes);
		TripConstraint constraint = factory.createConstraint(trips, availableModes);

		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "car", "car", "car", "car", "car"));
		Assert.assertEquals(-1, getNonFeasibleStage(constraint, trips, "car", "walk", "walk", "car", "car"));
		Assert.assertEquals(3, getNonFeasibleStage(constraint, trips, "car", "walk", "walk", "walk", "car"));
	}

	static int getNonFeasibleStage(TripConstraint constraint, List<ModeChoiceTrip> trips, String... modes) {
		List<String> modesList = Arrays.asList(modes);

		int i = 0;
		while (i < trips.size()) {
			if (!constraint.validateBeforeEstimation(trips.get(i), modesList.get(i), modesList.subList(0, i))) {
				return i;
			}

			i++;
		}

		return -1;
	}

	static private List<ModeChoiceTrip> createMockTrips(String... linkIds) {
		Person person = PopulationUtils.getFactory().createPerson(Id.createPersonId(""));

		List<Trip> trips = new ArrayList<>(linkIds.length - 1);

		for (int i = 0; i < linkIds.length - 1; i++) {
			String originActivityType = "";

			if (linkIds[i].startsWith("home")) {
				originActivityType = "home";
			} else if (linkIds[i].startsWith("outside")) {
				originActivityType = "outside";
			} else if (linkIds[i].startsWith("other")) {
				originActivityType = "other";
			} else {
				throw new IllegalStateException();
			}

			String destinationActivityType = "";

			if (linkIds[i + 1].startsWith("home")) {
				destinationActivityType = "home";
			} else if (linkIds[i + 1].startsWith("outside")) {
				destinationActivityType = "outside";
			} else if (linkIds[i + 1].startsWith("other")) {
				destinationActivityType = "other";
			} else {
				throw new IllegalStateException();
			}

			Activity originActivity = PopulationUtils.getFactory().createActivityFromLinkId(originActivityType,
					Id.createLinkId(linkIds[i]));
			Activity destinationActivity = PopulationUtils.getFactory()
					.createActivityFromLinkId(destinationActivityType, Id.createLinkId(linkIds[i + 1]));
			Leg leg = PopulationUtils.getFactory().createLeg("");

			trips.add(TripStructureUtils
					.getTrips(Arrays.asList(originActivity, leg, destinationActivity), new StageActivityTypesImpl())
					.get(0));
		}

		List<ModeChoiceTrip> modeChoiceTrips = new ArrayList<>(linkIds.length - 1);

		for (Trip trip : trips) {
			modeChoiceTrips.add(new DefaultModeChoiceTrip(person, trips, trip, ""));
		}

		return modeChoiceTrips;
	}
}
