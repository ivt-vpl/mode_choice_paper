package ch.ethz.matsim.papers.mode_choice_paper.tracking;

public class TrackedPrediction {
	public final double tripTravelTime;
	public final int numberOfLinks;

	public TrackedPrediction(double tripTravelTime, int numberOfLinks) {
		this.tripTravelTime = tripTravelTime;
		this.numberOfLinks = numberOfLinks;
	}
}
