package ch.ethz.matsim.papers.mode_choice_paper.estimation.other;

import java.util.List;

import org.matsim.core.population.PersonUtils;

import ch.ethz.matsim.mode_choice.estimation.ModalTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripCandidateWithPrediction;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.mode_choice.prediction.TeleportationPrediction;
import ch.ethz.matsim.mode_choice.prediction.TeleportationPredictor;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.CustomModeChoiceParameters;

public class CustomBikeEstimator implements ModalTripEstimator {
	final private TeleportationPredictor predictor;
	final private CustomModeChoiceParameters parameters;

	public CustomBikeEstimator(CustomModeChoiceParameters parameters, TeleportationPredictor predictor) {
		this.predictor = predictor;
		this.parameters = parameters;
	}

	@Override
	public TripCandidate estimateTrip(ModeChoiceTrip trip, List<TripCandidate> preceedingTrips) {
		TeleportationPrediction prediction = predictor.predict(trip);
		double travelTime_min = prediction.travelTime / 60.0;
		double age = PersonUtils.getAge(trip.getPerson());

		double utility = 0.0;
		utility += parameters.alphaBike;
		utility += parameters.betaTravelTimeBike_min * travelTime_min;
		utility += parameters.betaAgeBike_yr * Math.max(0.0, age - 18.0);

		return new TripCandidateWithPrediction(utility, "bike", prediction);
	}
}
