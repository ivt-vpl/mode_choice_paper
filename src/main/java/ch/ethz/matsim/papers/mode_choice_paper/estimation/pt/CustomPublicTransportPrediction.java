package ch.ethz.matsim.papers.mode_choice_paper.estimation.pt;

public class CustomPublicTransportPrediction {
	final public int numberOfTransfers;
	final public boolean isOnlyWalk;
	final public double transferTime;
	final public double accessEgressTime;
	final public double inVehicleTime;
	final public double inVehicleDistance;
	final public double crowflyDistance;

	public CustomPublicTransportPrediction(double inVehicleTime, double inVehicleDistance, double accessEgressTime,
			double transferTime, int numberOfTransfers, boolean isOnlyWalk, double crowflyDistance) {
		this.inVehicleTime = inVehicleTime;
		this.inVehicleDistance = inVehicleDistance;
		this.accessEgressTime = accessEgressTime;
		this.transferTime = transferTime;
		this.numberOfTransfers = numberOfTransfers;
		this.isOnlyWalk = isOnlyWalk;
		this.crowflyDistance = crowflyDistance;
	}
}
