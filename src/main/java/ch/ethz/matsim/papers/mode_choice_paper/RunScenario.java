package ch.ethz.matsim.papers.mode_choice_paper;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.pt.PtConstants;

import ch.ethz.matsim.baseline_scenario.BaselineModule;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.traffic.BaselineTrafficModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.baseline_scenario.zurich.ZurichModule;
import ch.ethz.matsim.papers.mode_choice_paper.travel_time.EstimatedTravelTimeModule;
import ch.ethz.matsim.papers.mode_choice_paper.utils.LongPlanFilter;
import ch.ethz.matsim.papers.mode_choice_paper.utils.RemoveRoutes;

public class RunScenario {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.allowPositionalArguments(false) //
				.requireOptions("config-path") //
				.allowOptions("config_path", "model-type", "no-vehicle-constraint", "track-car-travel-times",
						"use-advanced-vehicle-trip-constraint", "fallback-behaviour", "short-distance", "remove-routes",
						"use-custom-travel-time", "stuck-penalty") //
				.allowPrefixes("scoring") //
				.build();

		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("config-path"));
		cmd.applyConfiguration(config);

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		new LongPlanFilter(10, new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE))
				.run(scenario.getPopulation());

		if (cmd.hasOption("remove-routes")) {
			new RemoveRoutes().run(scenario.getPopulation());
		}

		Controler controler = new Controler(scenario);

		controler.addOverridingModule(new BaselineModule());
		controler.addOverridingModule(new BaselineTransitModule());
		controler.addOverridingModule(new BaselineTrafficModule(3.0));
		controler.addOverridingModule(new ZurichModule());
		controler.addOverridingModule(new CustomModeChoiceModule(cmd));

		if (cmd.hasOption("use-custom-travel-time")) {
			double stuckPenalty = cmd.getOption("stuck-penalty").map(Double::parseDouble)
					.orElse(config.qsim().getStuckTime());
			controler.addOverridingModule(new EstimatedTravelTimeModule(stuckPenalty));
		}

		controler.run();
	}
}
