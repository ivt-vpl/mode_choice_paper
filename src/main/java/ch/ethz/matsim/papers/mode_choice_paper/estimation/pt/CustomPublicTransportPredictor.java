package ch.ethz.matsim.papers.mode_choice_paper.estimation.pt;

import java.util.List;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.TripStructureUtils.Trip;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;

import ch.ethz.matsim.baseline_scenario.transit.routing.EnrichedTransitRoute;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;

public class CustomPublicTransportPredictor {
	final private TripRouter router;
	final private ActivityFacilities facilities;
	final private Network network;

	public CustomPublicTransportPredictor(TripRouter router, ActivityFacilities facilities, Network network) {
		this.router = router;
		this.facilities = facilities;
		this.network = network;
	}

	public CustomPublicTransportPrediction predict(ModeChoiceTrip trip) {
		Trip tripInformation = trip.getTripInformation();

		ActivityFacility originFacility = facilities.getFacilities()
				.get(tripInformation.getOriginActivity().getFacilityId());
		ActivityFacility destinationFacility = facilities.getFacilities()
				.get(tripInformation.getDestinationActivity().getFacilityId());

		LinkWrapperFacility newOriginFacility = new LinkWrapperFacility(
				network.getLinks().get(tripInformation.getOriginActivity().getLinkId()));
		LinkWrapperFacility newDestinationFacility = new LinkWrapperFacility(
				network.getLinks().get(tripInformation.getDestinationActivity().getLinkId()));

		List<Leg> legs = router
				.calcRoute("pt", newOriginFacility, newDestinationFacility,
						tripInformation.getOriginActivity().getEndTime(), trip.getPerson())
				.stream().filter(Leg.class::isInstance).map(Leg.class::cast).collect(Collectors.toList());

		double crowflyDistance = CoordUtils.calcEuclideanDistance(originFacility.getCoord(),
				destinationFacility.getCoord());

		boolean isOnlyWalk = true;
		int numberOfTransfers = -1;

		double transferTime = 0.0;

		double inVehicleTime = 0.0;
		double inVehicleDistance = 0.0;

		double accessEgressTime = 0.0;

		for (Leg leg : legs) {
			if (leg.getMode().equals("pt")) {
				numberOfTransfers++;

				EnrichedTransitRoute route = (EnrichedTransitRoute) leg.getRoute();

				inVehicleTime += route.getInVehicleTime();
				inVehicleDistance += route.getDistance();

				if (isOnlyWalk) {
					// This is the first PT leg. Only score 60s of waiting time here!
					transferTime += Math.min(route.getWaitingTime(), 60.0);
				} else {
					transferTime += route.getWaitingTime();
				}

				isOnlyWalk = false;
			} else if (leg.getMode().contains("walk")) {
				accessEgressTime += leg.getTravelTime();
			} else {
				throw new IllegalStateException("Can only enrich pt and *_walk legs");
			}
		}

		numberOfTransfers = Math.max(0, numberOfTransfers);

		return new CustomPublicTransportPrediction(inVehicleTime, inVehicleDistance, accessEgressTime, transferTime,
				numberOfTransfers, isOnlyWalk, crowflyDistance);
	}
}
