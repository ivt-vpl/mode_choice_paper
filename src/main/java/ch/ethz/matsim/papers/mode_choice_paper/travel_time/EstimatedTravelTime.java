package ch.ethz.matsim.papers.mode_choice_paper.travel_time;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.LinkLeaveEvent;
import org.matsim.api.core.v01.events.VehicleLeavesTrafficEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.LinkLeaveEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleLeavesTrafficEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.mobsim.framework.events.MobsimBeforeCleanupEvent;
import org.matsim.core.mobsim.framework.listeners.MobsimBeforeCleanupListener;
import org.matsim.core.router.util.TravelTime;
import org.matsim.vehicles.Vehicle;

public class EstimatedTravelTime implements TravelTime, LinkEnterEventHandler, LinkLeaveEventHandler,
		VehicleLeavesTrafficEventHandler, MobsimBeforeCleanupListener {
	private final Map<Id<Link>, List<Double>> cumulativeTraversalTimes = new HashMap<>();
	private final Map<Id<Link>, List<Long>> traversalCounts = new HashMap<>();
	private final Map<Id<Vehicle>, LinkEnterEvent> pendingEnterEvents = new HashMap<>();

	private final Map<Id<Link>, List<Double>> defaultTravelTimes = new HashMap<>();
	private final Map<Id<Link>, List<Double>> estimatedTravelTimes = new HashMap<>();

	private final double stuckPenalty;

	private double startTime;
	private double endTime;
	private double binsize;
	private int numberOfBins;

	public EstimatedTravelTime(Network network, double startTime, double endTime, double binsize,
			TravelTime initialTravelTime, double stuckPenalty) {
		this.numberOfBins = (int) Math.ceil((endTime - startTime) / binsize);

		this.startTime = startTime;
		this.binsize = binsize;
		this.endTime = numberOfBins * binsize;
		this.stuckPenalty = stuckPenalty;

		for (Id<Link> linkId : network.getLinks().keySet()) {
			List<Double> cumulativeTravelTimes = new ArrayList<>(Collections.nCopies(numberOfBins, 0.0));
			cumulativeTraversalTimes.put(linkId, cumulativeTravelTimes);

			List<Long> linkTraversalCounts = new ArrayList<>(Collections.nCopies(numberOfBins, 1L));
			traversalCounts.put(linkId, linkTraversalCounts);

			List<Double> linkDefaultTravelTimes = new ArrayList<>(Collections.nCopies(numberOfBins, 0.0));
			defaultTravelTimes.put(linkId, linkDefaultTravelTimes);

			List<Double> linkEstimatedTravelTimes = new ArrayList<>(Collections.nCopies(numberOfBins, 0.0));
			estimatedTravelTimes.put(linkId, linkEstimatedTravelTimes);
		}

		for (Link link : network.getLinks().values()) {
			List<Double> linkDefaultTravelTimes = defaultTravelTimes.get(link.getId());

			for (int i = 0; i < numberOfBins; i++) {
				double midTime = startTime + binsize * (i + 0.5);
				linkDefaultTravelTimes.set(i, initialTravelTime.getLinkTravelTime(link, midTime, null, null));
			}
		}

		performEstimation();
	}

	private void performEstimation() {
		for (LinkEnterEvent enterEvent : pendingEnterEvents.values()) {
			int startIndex = computeNormalizedIndex(enterEvent.getTime());
			List<Double> linkCumulativeTraversalTimes = cumulativeTraversalTimes.get(enterEvent.getLinkId());
			List<Long> linkTraversalCounts = traversalCounts.get(enterEvent.getLinkId());

			for (int i = startIndex; i < numberOfBins; i++) {
				linkTraversalCounts.set(i, linkTraversalCounts.get(i) + 1);
				linkCumulativeTraversalTimes.set(i, linkCumulativeTraversalTimes.get(i) + stuckPenalty);
			}
		}

		pendingEnterEvents.clear();

		for (Id<Link> linkId : cumulativeTraversalTimes.keySet()) {
			List<Double> linkCumulativeTraversalTimes = cumulativeTraversalTimes.get(linkId);
			List<Long> linkTraversalCounts = traversalCounts.get(linkId);

			List<Double> linkDefaultTravelTimes = defaultTravelTimes.get(linkId);
			List<Double> linkEstimatedTravelTimes = estimatedTravelTimes.get(linkId);

			for (int i = 0; i < numberOfBins; i++) {
				long traversalCount = linkTraversalCounts.get(i);
				double cumulativeTravelTime = linkCumulativeTraversalTimes.get(i);

				if (traversalCount > 0) {
					linkEstimatedTravelTimes.set(i, cumulativeTravelTime / traversalCount);
				} else {
					linkEstimatedTravelTimes.set(i, linkDefaultTravelTimes.get(i));
				}

				linkCumulativeTraversalTimes.set(i, 0.0);
				linkTraversalCounts.set(i, 0L);
			}
		}
	}

	private int computeIndex(double time) {
		if (time < startTime) {
			throw new IllegalStateException();
		}

		if (time >= endTime) {
			throw new IllegalStateException();
		}

		return (int) Math.floor((time - startTime) / binsize);
	}

	private int computeNormalizedIndex(double time) {
		if (time < startTime) {
			return 0;
		}

		if (time >= endTime) {
			return numberOfBins - 1;
		}

		return computeIndex(time);
	}

	@Override
	public void handleEvent(LinkEnterEvent event) {
		pendingEnterEvents.put(event.getVehicleId(), event);
	}

	@Override
	public void handleEvent(VehicleLeavesTrafficEvent event) {
		processLinkLeaveTime(event.getVehicleId(), event.getLinkId(), event.getTime());
	}

	@Override
	public void handleEvent(LinkLeaveEvent event) {
		processLinkLeaveTime(event.getVehicleId(), event.getLinkId(), event.getTime());
	}

	private void processLinkLeaveTime(Id<Vehicle> vehicleId, Id<Link> linkId, double leaveTime) {
		LinkEnterEvent enterEvent = pendingEnterEvents.remove(vehicleId);

		if (enterEvent != null) {
			double traversalTime = leaveTime - enterEvent.getTime();

			int startIndex = computeNormalizedIndex(enterEvent.getTime());
			int endIndex = computeNormalizedIndex(leaveTime);

			List<Double> linkCumulativeTraversalTimes = cumulativeTraversalTimes.get(linkId);
			List<Long> linkTraversalCounts = traversalCounts.get(linkId);

			for (int i = startIndex; i <= endIndex; i++) {
				linkCumulativeTraversalTimes.set(i, linkCumulativeTraversalTimes.get(i) + traversalTime);
				linkTraversalCounts.set(i, linkTraversalCounts.get(i) + 1);
			}
		}
	}

	@Override
	public double getLinkTravelTime(Link link, double time, Person person, Vehicle vehicle) {
		int index = computeNormalizedIndex(time);
		return estimatedTravelTimes.get(link.getId()).get(index);
	}

	@Override
	public void notifyMobsimBeforeCleanup(@SuppressWarnings("rawtypes") MobsimBeforeCleanupEvent e) {
		performEstimation();
	}
}
