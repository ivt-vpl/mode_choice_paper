package ch.ethz.matsim.papers.mode_choice_paper.constraints;

import java.util.Collection;
import java.util.List;

import org.matsim.core.router.TripStructureUtils.Trip;

import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraint;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraintFactory;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;

/**
 * This constraint is a bit special. Let's say the scenario has been cut and a
 * long public transit journey ends right after the scenario boundary. In that
 * case maybe only the last egress walk part of the trip is existant in the
 * scenario. Hence, the constraint that forbids very short distances for public
 * transit legs for get active, or the constraint for only having public transit
 * trips that include at least one actual vehicular leg will get active. So if
 * these constraints become active we would arrive at an infeasible state,
 * because in the chain generation it has already been defined that this trip
 * *must* be public transport, because it comes from outside and is fix. Hence,
 * the *only* fix alternative (being public transport) would be filtered out by
 * the constraints and there would be no feasible choice! Therefore we use this
 * constraint wrapper to deactive any constraint if a fixed trip (i.e. either
 * entering the scenario or leaving the scenario) is considered.
 * 
 * @author Sebastian Hörl
 *
 */
public class OutsideConstraintWrapper implements TripConstraint {
	final private TripConstraint delegate;

	public OutsideConstraintWrapper(TripConstraint delegate) {
		this.delegate = delegate;
	}

	@Override
	public boolean validateBeforeEstimation(ModeChoiceTrip trip, String mode, List<String> previousModes) {
		if (isOutside(trip)) {
			return true;
		} else {
			return delegate.validateBeforeEstimation(trip, mode, previousModes);
		}
	}

	@Override
	public boolean validateAfterEstimation(ModeChoiceTrip trip, TripCandidate candidate,
			List<TripCandidate> previousCandidates) {
		if (isOutside(trip)) {
			return true;
		} else {
			return delegate.validateAfterEstimation(trip, candidate, previousCandidates);
		}
	}

	private boolean isOutside(ModeChoiceTrip trip) {
		Trip info = trip.getTripInformation();

		if (info.getOriginActivity().getType().equals("outside")) {
			return true;
		}

		if (info.getDestinationActivity().getType().equals("outside")) {
			return true;
		}

		if (trip.getInitialMode().equals("outside")) {
			return true;
		}

		return false;
	}

	static public class Factory implements TripConstraintFactory {
		final private TripConstraintFactory delegate;

		public Factory(TripConstraintFactory delegate) {
			this.delegate = delegate;
		}

		@Override
		public TripConstraint createConstraint(List<ModeChoiceTrip> trips, Collection<String> availableModes) {
			return new OutsideConstraintWrapper(delegate.createConstraint(trips, availableModes));
		}
	}
}
