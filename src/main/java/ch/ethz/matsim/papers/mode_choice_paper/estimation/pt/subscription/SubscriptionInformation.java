package ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.subscription;

public class SubscriptionInformation {
	final public boolean hasGA;
	final public boolean hasHalbtax;
	final public boolean hasVerbundabo;
	final public boolean hasNothing;

	public SubscriptionInformation(boolean hasGA, boolean hasHalbtax, boolean hasVerbundabo, boolean hasNothing) {
		this.hasGA = hasGA;
		this.hasHalbtax = hasHalbtax;
		this.hasVerbundabo = hasVerbundabo;
		this.hasNothing = hasNothing;
	}
}
