package ch.ethz.matsim.papers.mode_choice_paper.travel_time;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.groups.TravelTimeCalculatorConfigGroup;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;

import com.google.inject.Provides;
import com.google.inject.Singleton;

public class EstimatedTravelTimeModule extends AbstractModule {
	private final double stuckPenalty;

	public EstimatedTravelTimeModule(double stuckPenalty) {
		this.stuckPenalty = stuckPenalty;
	}

	@Override
	public void install() {
		addTravelTimeBinding("car").to(EstimatedTravelTime.class);
		addEventHandlerBinding().to(EstimatedTravelTime.class);
		addMobsimListenerBinding().to(EstimatedTravelTime.class);
	}

	@Provides
	@Singleton
	public EstimatedTravelTime provideEstimatedTravelTime(Network network, TravelTimeCalculatorConfigGroup config) {
		return new EstimatedTravelTime(network, 0.0, config.getMaxTime(), config.getTraveltimeBinSize(),
				new FreeSpeedTravelTime(), stuckPenalty);
	}
}
