package ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.subscription;

import org.matsim.api.core.v01.population.Person;
import org.matsim.utils.objectattributes.ObjectAttributes;

public class SubscriptionFinder {
	final private ObjectAttributes personAttributes;

	public SubscriptionFinder(ObjectAttributes personAttributes) {
		this.personAttributes = personAttributes;
	}

	public SubscriptionInformation getSubscriptions(Person person) {
		String seasonTicket = (String) personAttributes.getAttribute(person.getId().toString(), "season_ticket");

		boolean hasGA = false;
		boolean hasHalbtax = false;
		boolean hasVerbundabo = false;
		boolean hasNothing = true;

		if (seasonTicket != null) {
			hasGA = seasonTicket.contains("Generalabo");
			hasHalbtax = seasonTicket.contains("Halbtax");
			hasVerbundabo = seasonTicket.contains("Verbund");
			hasNothing = false;
		}

		return new SubscriptionInformation(hasGA, hasHalbtax, hasVerbundabo, hasNothing);
	}
}
