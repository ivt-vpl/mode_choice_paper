package ch.ethz.matsim.papers.mode_choice_paper.stuck;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.PersonStuckEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.PersonStuckEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class AnalyseStuckVehicles {
	static public void main(String[] args) throws ConfigurationException, IOException {
		CommandLine cmd = new CommandLine.Builder(args).requireOptions("events-path", "output-path").build();

		StuckHandler handler = new StuckHandler();

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(handler);

		new MatsimEventsReader(eventsManager).readFile(cmd.getOptionStrict("events-path"));

		Collections.sort(handler.stuckInfo, new Comparator<StuckInfo>() {
			@Override
			public int compare(StuckInfo o1, StuckInfo o2) {
				return Double.compare(o1.time, o2.time);
			}
		});

		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File(cmd.getOptionStrict("output-path")))));
		writer.write("time;link\n");

		for (StuckInfo info : handler.stuckInfo) {
			writer.write(String.format("%s;%f\n", info.linkId.toString(), info.time));
		}

		writer.close();
	}

	static public class StuckHandler implements LinkEnterEventHandler, PersonStuckEventHandler {
		private Map<Id<Person>, LinkEnterEvent> lastEnterEvents = new HashMap<>();
		public final List<StuckInfo> stuckInfo = new LinkedList<>();

		@Override
		public void handleEvent(LinkEnterEvent event) {
			lastEnterEvents.put(Id.createPersonId(event.getVehicleId()), event);
		}

		@Override
		public void handleEvent(PersonStuckEvent event) {
			if (event.getLegMode() != null && event.getLegMode().equals("car")) {
				LinkEnterEvent enterEvent = lastEnterEvents.get(event.getPersonId());

				if (enterEvent != null) {
					stuckInfo.add(new StuckInfo(enterEvent.getTime(), event.getLinkId()));
				}
			}
		}
	}

	static public class StuckInfo {
		public final double time;
		public final Id<Link> linkId;

		public StuckInfo(double time, Id<Link> linkId) {
			this.time = time;
			this.linkId = linkId;
		}
	}
}
