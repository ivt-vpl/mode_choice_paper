package ch.ethz.matsim.papers.mode_choice_paper.estimation;

import java.lang.reflect.Field;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.subscription.SubscriptionInformation;

public class CustomModeChoiceParameters {
	// Cost
	public double betaCostBase = -0.126;
	public double averageDistance_km = 40.0;
	public double lambdaDistanceCost = -0.4;

	public double betaCost(double distance_km) {
		distance_km = Math.max(distance_km, 0.001);
		return betaCostBase * Math.pow(distance_km / averageDistance_km, lambdaDistanceCost);
	}

	// Walk
	public double alphaWalk = 0.63;
	public double betaTravelTimeWalk_min = -0.141;

	// Bike
	public double alphaBike = -0.1;
	public double betaTravelTimeBike_min = -0.0805;
	public double betaAgeBike_yr = -0.0496;

	// Car
	public double alphaCar = 0.827;
	public double betaTravelTimeCar_min = -0.0667;

	public double distanceCostCar_km = 0.27;

	public double parkingSearchTimeCar_min = 6.0;
	public double accessEgressWalkTimeCar_min = 5.0;

	// Public Transport
	public double alphaPublicTransport = 0.0;
	public double betaNumberOfTransfersPublicTransport = -0.17;

	public double betaInVehicleTimePublicTransport_min = -0.0192;
	public double betaTransferTimePublicTransport_min = -0.0384;
	public double betaAccessEgressTimePublicTransport_min = -0.0804;

	private PublicTransportCostStructure costStructure = new PublicTransportCostStructure();
	private double minimumPublicTransportPriceWithoutSubscriptions = 2.7;
	private double minimumPublicTransportPriceWithHalbtax = 2.3;
	private double minimumPublicTransportPriceWithUnknownSubscription = 2.3;

	public double costPublicTransport(SubscriptionInformation subscriptions, double crowflyDistance_km) {
		if (subscriptions.hasNothing) {
			double costPerKm = costStructure.distanceCost_km(crowflyDistance_km);
			return Math.max(minimumPublicTransportPriceWithoutSubscriptions, costPerKm * crowflyDistance_km);
		} else if (subscriptions.hasGA) {
			return 0.0;
		} else if (subscriptions.hasVerbundabo) {
			double costPerKm = costStructure.distanceCostZVV_km(crowflyDistance_km);
			return costPerKm * crowflyDistance_km;
		} else if (subscriptions.hasHalbtax) {
			double costPerKm = costStructure.distanceCost_km(crowflyDistance_km) * 0.5;
			return Math.max(minimumPublicTransportPriceWithHalbtax, costPerKm * crowflyDistance_km);
		} else {
			double costPerKm = costStructure.distanceCost_km(crowflyDistance_km);
			return Math.max(minimumPublicTransportPriceWithUnknownSubscription, costPerKm * crowflyDistance_km);
		}
	}

	private static Logger logger = Logger.getLogger(CustomModeChoiceParameters.class);

	public void load(Map<String, String> parameters) {
		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			try {
				Field field = CustomModeChoiceParameters.class.getField(entry.getKey());
				field.set(this, Double.valueOf(entry.getValue()));
				logger.info(String.format("Set %s = %f", entry.getKey(), Double.valueOf(entry.getValue())));
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
