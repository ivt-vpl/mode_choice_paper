package ch.ethz.matsim.papers.mode_choice_paper.estimation.pt;

import java.util.List;

import ch.ethz.matsim.mode_choice.estimation.ModalTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripCandidateWithPrediction;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.CustomModeChoiceParameters;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.subscription.SubscriptionFinder;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.subscription.SubscriptionInformation;

public class CustomPublicTransportEstimator implements ModalTripEstimator {
	final private CustomPublicTransportPredictor predictor;
	final private CustomModeChoiceParameters parameters;
	final private SubscriptionFinder subscriptionFinder;

	public CustomPublicTransportEstimator(CustomModeChoiceParameters parameters,
			CustomPublicTransportPredictor predictor, SubscriptionFinder subscriptionFinder) {
		this.predictor = predictor;
		this.parameters = parameters;
		this.subscriptionFinder = subscriptionFinder;
	}

	@Override
	public TripCandidate estimateTrip(ModeChoiceTrip trip, List<TripCandidate> preceedingTrips) {
		CustomPublicTransportPrediction prediction = predictor.predict(trip);
		SubscriptionInformation subscriptions = subscriptionFinder.getSubscriptions(trip.getPerson());

		double crowflyDistance_km = prediction.crowflyDistance * 1e-3;
		double inVehicleTime_min = prediction.inVehicleTime / 60.0;
		double transferTime_min = prediction.transferTime / 60.0;
		double accessEgressTime_min = prediction.accessEgressTime / 60.0;
		double inVehicleDistance_km = prediction.inVehicleDistance * 1e-3;

		double cost = parameters.costPublicTransport(subscriptions, crowflyDistance_km);
		double betaCost = parameters.betaCost(inVehicleDistance_km);

		double utility = 0.0;
		utility += parameters.alphaPublicTransport;
		utility += parameters.betaNumberOfTransfersPublicTransport * prediction.numberOfTransfers;

		utility += parameters.betaInVehicleTimePublicTransport_min * inVehicleTime_min;
		utility += parameters.betaTransferTimePublicTransport_min * transferTime_min;
		utility += parameters.betaAccessEgressTimePublicTransport_min * accessEgressTime_min;

		utility += betaCost * cost;

		return new TripCandidateWithPrediction(utility, "pt", prediction);
	}
}
