package ch.ethz.matsim.papers.mode_choice_paper;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.config.groups.PlansCalcRouteConfigGroup;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.costcalculators.TravelDisutilityFactory;
import org.matsim.core.router.util.TravelTime;
import org.matsim.facilities.ActivityFacilities;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.traffic.BaselineLinkSpeedCalculator;
import ch.ethz.matsim.mode_choice.constraints.CompositeTourConstraintFactory;
import ch.ethz.matsim.mode_choice.constraints.CompositeTripConstraintFactory;
import ch.ethz.matsim.mode_choice.constraints.TourConstraintFromTripConstraint;
import ch.ethz.matsim.mode_choice.estimation.ModeAwareTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TourEstimatorFromTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripEstimatorCache;
import ch.ethz.matsim.mode_choice.framework.ModeAvailability;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceModel;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceModel.FallbackBehaviour;
import ch.ethz.matsim.mode_choice.framework.plan_based.PlanBasedModel;
import ch.ethz.matsim.mode_choice.framework.tour_based.ActivityTourFinder;
import ch.ethz.matsim.mode_choice.framework.tour_based.TourBasedModel;
import ch.ethz.matsim.mode_choice.framework.tour_based.TourFinder;
import ch.ethz.matsim.mode_choice.framework.tour_based.estimation.TourCandidate;
import ch.ethz.matsim.mode_choice.framework.tour_based.estimation.TourEstimator;
import ch.ethz.matsim.mode_choice.framework.trip_based.TripBasedModel;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraintFactory;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.mode_choice.framework.utilities.MultinomialSelector;
import ch.ethz.matsim.mode_choice.framework.utilities.UtilitySelectorFactory;
import ch.ethz.matsim.mode_choice.framework.utils.ModeChainGeneratorFactory;
import ch.ethz.matsim.mode_choice.mode_availability.LicenseAndOwnershipModeAvailability;
import ch.ethz.matsim.mode_choice.prediction.TeleportationPredictor;
import ch.ethz.matsim.mode_choice.replanning.ModeChoiceModelStrategy;
import ch.ethz.matsim.mode_choice.replanning.NonSelectedPlanSelector;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.AdvancedVehicleTripConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.AvoidOnlyWalkConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.CustomHybridConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.OutsideConstraintWrapper;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.OutsideTripConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.ShortDistanceConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.VehicleTourConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.constraints.VehicleTripConstraint;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.CustomModeChoiceParameters;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.car.CustomCarEstimator;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.car.CustomCarPredictor;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.other.CustomBikeEstimator;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.other.CustomOutsideEstimator;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.other.CustomWalkEstimator;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.CustomPublicTransportEstimator;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.CustomPublicTransportPredictor;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.pt.subscription.SubscriptionFinder;
import ch.ethz.matsim.papers.mode_choice_paper.tracking.TrackingModeChoiceModel;
import ch.ethz.matsim.papers.mode_choice_paper.tracking.TravelTimeTracker;
import ch.ethz.matsim.papers.mode_choice_paper.tracking.TravelTimeTrackerListener;

public class CustomModeChoiceModule extends AbstractModule {
	private static final List<String> MODES = Arrays.asList("car", "pt", "bike", "walk", "outside");
	private static final List<String> CACHED_MODES = Arrays.asList("car", "pt", "bike", "walk");

	private final CommandLine cmd;
	private final List<String> vehicleModes = new LinkedList<>();

	public CustomModeChoiceModule(CommandLine cmd) {
		this.cmd = cmd;

		if (!cmd.hasOption("no-vehicle-constraint")) {
			this.vehicleModes.addAll(Arrays.asList("car", "bike"));
		}
	}

	@Override
	public void install() {
		addPlanStrategyBinding("custom").toProvider(ModeChoiceModelStrategy.class);
		bindPlanSelectorForRemoval().to(NonSelectedPlanSelector.class);

		// TODO: Why is this not working on ifalik?
		// addControlerListenerBinding().to(RevisionWriter.class);

		if (cmd.hasOption("track-car-travel-times")) {
			addControlerListenerBinding().to(TravelTimeTrackerListener.class);
			addEventHandlerBinding().to(TravelTimeTrackerListener.class);
		}

		bind(TravelTimeTracker.class);

		addTravelDisutilityFactoryBinding("car").to(CustomCarDisutility.Factory.class);

		// TODO: Probably can be completely kicked out soon
		// addEventHandlerBinding().to(FreeflowTravelTimeValidator.class);
	}

	@Provides
	@Singleton
	public CustomModeChoiceParameters provideCustomModeChoiceParameters() throws ConfigurationException {
		String prefix = "scoring:";

		List<String> scoringOptions = cmd.getAvailableOptions().stream().filter(o -> o.startsWith(prefix))
				.collect(Collectors.toList());

		Map<String, String> rawParameters = new HashMap<>();

		for (String option : scoringOptions) {
			rawParameters.put(option.substring(prefix.length()), cmd.getOptionStrict(option));
		}

		CustomModeChoiceParameters parameters = new CustomModeChoiceParameters();
		parameters.load(rawParameters);

		return parameters;
	}

	@Provides
	@Singleton
	public SubscriptionFinder provideSubscriptionFinder(Population population) {
		return new SubscriptionFinder(population.getPersonAttributes());
	}

	@Provides
	@Named("road")
	@Singleton
	public Network provideRoadNetwork(Network fullNetwork) {
		Network roadNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(fullNetwork).filter(roadNetwork, Collections.singleton("car"));
		return roadNetwork;
	}

	@Provides
	@Singleton
	public CustomCarDisutility.Factory provideCustomCarDisutilityFactory(BaselineLinkSpeedCalculator speedCalculator) {
		return new CustomCarDisutility.Factory(speedCalculator);
	}

	@Provides
	public ModeChoiceModel provideModeChoiceModel(PlansCalcRouteConfigGroup routeConfig,
			CustomModeChoiceParameters parameters, TripRouter router, SubscriptionFinder subscriptionFinder,
			ActivityFacilities facilities, Network network, TravelTimeTracker travelTimeTracker,
			@Named("car") TravelTime carTravelTime, @Named("car") TravelDisutilityFactory carTravelDisutilityFactory)
			throws ConfigurationException {
		ModeAvailability modeAvailability = new LicenseAndOwnershipModeAvailability(MODES);

		double crowflyDistanceFactorWalk = routeConfig.getModeRoutingParams().get("walk").getBeelineDistanceFactor();
		double speedWalk = routeConfig.getModeRoutingParams().get("walk").getTeleportedModeSpeed();

		TeleportationPredictor teleportationPredictorWalk = new TeleportationPredictor(crowflyDistanceFactorWalk,
				speedWalk);
		CustomWalkEstimator walkEstimator = new CustomWalkEstimator(parameters, teleportationPredictorWalk);

		double crowflyDistanceFactorBike = routeConfig.getModeRoutingParams().get("bike").getBeelineDistanceFactor();
		double speedBike = routeConfig.getModeRoutingParams().get("bike").getTeleportedModeSpeed();

		TeleportationPredictor teleportationPredictorBike = new TeleportationPredictor(crowflyDistanceFactorBike,
				speedBike);
		CustomBikeEstimator bikeEstimator = new CustomBikeEstimator(parameters, teleportationPredictorBike);

		CustomPublicTransportPredictor publicTransportPredictor = new CustomPublicTransportPredictor(router, facilities,
				network);
		CustomPublicTransportEstimator publicTransportEstimator = new CustomPublicTransportEstimator(parameters,
				publicTransportPredictor, subscriptionFinder);

		CustomCarPredictor carPredictor = new CustomCarPredictor(router, facilities,
				carTravelDisutilityFactory.createTravelDisutility(carTravelTime), network);
		CustomCarEstimator carEstimator = new CustomCarEstimator(parameters, carPredictor);

		ModeAwareTripEstimator modeAwareEstimator = new ModeAwareTripEstimator();
		modeAwareEstimator.addEstimator("walk", walkEstimator);
		modeAwareEstimator.addEstimator("bike", bikeEstimator);
		modeAwareEstimator.addEstimator("pt", publicTransportEstimator);
		modeAwareEstimator.addEstimator("car", carEstimator);
		modeAwareEstimator.addEstimator("outside", new CustomOutsideEstimator());

		TripEstimatorCache estimator = new TripEstimatorCache(modeAwareEstimator, CACHED_MODES);

		// CONSTRAINTS

		double shortDistance = cmd.getOption("short-distance").map(Double::parseDouble)
				.orElse(ShortDistanceConstraint.DEFAULT_SHORT_DISTANCE);

		CompositeTourConstraintFactory tourConstraintFactory = new CompositeTourConstraintFactory();
		tourConstraintFactory.addFactory(new TourConstraintFromTripConstraint.Factory(
				new OutsideConstraintWrapper.Factory(new AvoidOnlyWalkConstraint.Factory())));
		tourConstraintFactory.addFactory(new TourConstraintFromTripConstraint.Factory(
				new OutsideConstraintWrapper.Factory(new ShortDistanceConstraint.Factory(shortDistance))));
		tourConstraintFactory.addFactory(new VehicleTourConstraint.Factory(vehicleModes));

		CompositeTripConstraintFactory tripConstraintFactory = new CompositeTripConstraintFactory();
		tripConstraintFactory.addFactory(new OutsideConstraintWrapper.Factory(new AvoidOnlyWalkConstraint.Factory()));
		tripConstraintFactory.addFactory(new OutsideTripConstraint.Factory());

		boolean useAdvancedVehicleTripConstraint = cmd.hasOption("use-advanced-vehicle-trip-constraint");
		TripConstraintFactory shortDistanceConstraintFactory = new OutsideConstraintWrapper.Factory(
				new ShortDistanceConstraint.Factory(shortDistance));

		if (useAdvancedVehicleTripConstraint) {
			tripConstraintFactory.addFactory(new CustomHybridConstraint.Factory(
					new AdvancedVehicleTripConstraint.Factory(vehicleModes), shortDistanceConstraintFactory));
		} else {
			tripConstraintFactory.addFactory(shortDistanceConstraintFactory);
			tripConstraintFactory.addFactory(new VehicleTripConstraint.Factory(vehicleModes));
		}

		TourEstimator tourEstimator = new TourEstimatorFromTripEstimator(estimator);
		ModeChainGeneratorFactory modeChainGeneratorFactory = new CustomModeChainGenerator.Factory();

		TourFinder tourFinder = new ActivityTourFinder("home");

		UtilitySelectorFactory<TripCandidate> tripSelectorFactory = new MultinomialSelector.Factory<>(700.0);
		UtilitySelectorFactory<TourCandidate> tourSelectorFactory = new MultinomialSelector.Factory<>(700.0);

		String modelType = cmd.getOptionStrict("model-type");
		FallbackBehaviour fallbackBehaviour = cmd.getOption("fallback-behaviour").map(FallbackBehaviour::valueOf)
				.orElse(FallbackBehaviour.INITIAL_CHOICE);

		ModeChoiceModel model;

		switch (modelType) {
		case "trip":
			model = new TripBasedModel(estimator, modeAvailability, tripConstraintFactory, tripSelectorFactory,
					fallbackBehaviour);
			break;
		case "tour":
			model = new TourBasedModel(tourEstimator, modeAvailability, tourConstraintFactory, tourFinder,
					tourSelectorFactory, modeChainGeneratorFactory, fallbackBehaviour);
			break;
		case "plan":
			model = new PlanBasedModel(tourEstimator, modeAvailability, tourConstraintFactory, tourSelectorFactory,
					modeChainGeneratorFactory, fallbackBehaviour);
			break;
		default:
			throw new IllegalStateException("Unknown model type: " + modelType);
		}

		if (cmd.hasOption("track-car-travel-times")) {
			model = new TrackingModeChoiceModel(model, travelTimeTracker);
		}

		return model;
	}

}
