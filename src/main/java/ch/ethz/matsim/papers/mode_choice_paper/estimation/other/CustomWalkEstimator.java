package ch.ethz.matsim.papers.mode_choice_paper.estimation.other;

import java.util.List;

import ch.ethz.matsim.mode_choice.estimation.ModalTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripCandidateWithPrediction;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.mode_choice.prediction.TeleportationPrediction;
import ch.ethz.matsim.mode_choice.prediction.TeleportationPredictor;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.CustomModeChoiceParameters;

public class CustomWalkEstimator implements ModalTripEstimator {
	final private TeleportationPredictor predictor;
	final private CustomModeChoiceParameters parameters;

	public CustomWalkEstimator(CustomModeChoiceParameters parameters, TeleportationPredictor predictor) {
		this.predictor = predictor;
		this.parameters = parameters;
	}

	@Override
	public TripCandidate estimateTrip(ModeChoiceTrip trip, List<TripCandidate> preceedingTrips) {
		TeleportationPrediction prediction = predictor.predict(trip);

		double travelTime_min = prediction.travelTime / 60.0;

		double utility = parameters.alphaWalk;
		utility += parameters.betaTravelTimeWalk_min * travelTime_min;

		return new TripCandidateWithPrediction(utility, "walk", prediction);
	}
}
