package ch.ethz.matsim.papers.mode_choice_paper.constraints;

import java.util.Collection;
import java.util.List;

import ch.ethz.matsim.mode_choice.constraints.AbstractTripConstraint;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraint;
import ch.ethz.matsim.mode_choice.framework.trip_based.constraints.TripConstraintFactory;

public class OutsideTripConstraint extends AbstractTripConstraint {
	@Override
	public boolean validateBeforeEstimation(ModeChoiceTrip trip, String mode, List<String> previousModes) {
		if (trip.getInitialMode().equals("outside")) {
			return mode.equals("outside");
		} else if (mode.equals("outside")) {
			return trip.getInitialMode().equals("outside");
		}

		return true;
	}

	static public class Factory implements TripConstraintFactory {
		@Override
		public TripConstraint createConstraint(List<ModeChoiceTrip> trips, Collection<String> availableModes) {
			return new OutsideTripConstraint();
		}
	}
}
