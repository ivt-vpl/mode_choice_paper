package ch.ethz.matsim.papers.mode_choice_paper;

import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.ConfigWriter;
import org.matsim.core.config.groups.QSimConfigGroup.TrafficDynamics;
import org.matsim.core.config.groups.StrategyConfigGroup.StrategySettings;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class MakeScenario {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.allowPositionalArguments(false) //
				.requireOptions("original-config-path", "output-config-path")
				.build();
		
		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("original-config-path"));
		
		config.plansCalcRoute().getOrCreateModeRoutingParams("bike").setTeleportedModeSpeed(10.0 * 1000 / 3600.0);
		config.plansCalcRoute().getOrCreateModeRoutingParams("bike").setBeelineDistanceFactor(1.4);

		config.plansCalcRoute().getOrCreateModeRoutingParams("walk").setTeleportedModeSpeed(5.0 * 1000 / 3600.0);
		config.plansCalcRoute().getOrCreateModeRoutingParams("walk").setBeelineDistanceFactor(1.05);

		config.plansCalcRoute().getOrCreateModeRoutingParams("access_walk").setTeleportedModeSpeed(5.0 * 1000 / 3600.0);
		config.plansCalcRoute().getOrCreateModeRoutingParams("access_walk").setBeelineDistanceFactor(1.05);

		config.plansCalcRoute().getOrCreateModeRoutingParams("egress_walk").setTeleportedModeSpeed(5.0 * 1000 / 3600.0);
		config.plansCalcRoute().getOrCreateModeRoutingParams("egress_walk").setBeelineDistanceFactor(1.05);

		config.strategy().setMaxAgentPlanMemorySize(1);
		
		config.qsim().setFlowCapFactor(0.1);
		config.qsim().setStorageCapFactor(1.0);
		config.qsim().setTrafficDynamics(TrafficDynamics.kinematicWaves);
		
		config.strategy().clearStrategySettings();
		config.strategy().setMaxAgentPlanMemorySize(1);

		StrategySettings strategy;
		
		// See MATSIM-766 (https://matsim.atlassian.net/browse/MATSIM-766)
		strategy = new StrategySettings();
		strategy.setStrategyName("SubtourModeChoice");
		strategy.setDisableAfter(0);
		strategy.setWeight(0.0);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("custom");
		strategy.setWeight(0.15);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("ReRoute");
		strategy.setWeight(0.05);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("KeepLastSelected");
		strategy.setWeight(0.85);
		config.strategy().addStrategySettings(strategy);
		
		new ConfigWriter(config).write(cmd.getOptionStrict("output-config-path"));
	}
}
