package ch.ethz.matsim.papers.mode_choice_paper.estimation.car;

import java.util.List;

import ch.ethz.matsim.mode_choice.estimation.ModalTripEstimator;
import ch.ethz.matsim.mode_choice.estimation.TripCandidateWithPrediction;
import ch.ethz.matsim.mode_choice.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.framework.trip_based.estimation.TripCandidate;
import ch.ethz.matsim.papers.mode_choice_paper.estimation.CustomModeChoiceParameters;

public class CustomCarEstimator implements ModalTripEstimator {
	final private CustomCarPredictor predictor;
	final private CustomModeChoiceParameters parameters;

	public CustomCarEstimator(CustomModeChoiceParameters parameters, CustomCarPredictor predictor) {
		this.predictor = predictor;
		this.parameters = parameters;
	}

	@Override
	public TripCandidate estimateTrip(ModeChoiceTrip trip, List<TripCandidate> preceedingTrips) {
		CustomCarPrediction prediction = predictor.predict(trip);
		double travelTime_min = prediction.travelTime / 60.0;
		double distance_km = prediction.distance * 1e-3;

		double cost = parameters.distanceCostCar_km * distance_km;
		double betaCost = parameters.betaCost(distance_km);

		double utility = 0.0;
		utility += parameters.alphaCar;

		utility += parameters.betaTravelTimeCar_min * travelTime_min;
		utility += parameters.betaTravelTimeCar_min * parameters.parkingSearchTimeCar_min;
		utility += parameters.betaTravelTimeWalk_min * parameters.accessEgressWalkTimeCar_min;

		utility += betaCost * cost;

		return new TripCandidateWithPrediction(utility, "car", prediction);
	}
}
