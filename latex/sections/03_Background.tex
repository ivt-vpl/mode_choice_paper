\section{Background}

Powerful tools for the simulation of the transport system exist. However, most
of them assume a static demand, i.e. a fixed set of trips or flows that need
to be handled by the transport system. The emergence of agent-based transport
simulation has made it possible also to consider feedback loops between supply
and demand in more detail. For instance, if too many people use a car in the system, travel
times will go up, so people will try to switch to other modes of transport
until an equilibrium between supply and demand is found. Lately, research is
ongoing on the topic of combining agent-based transport simulations (which
can produce network conditions given a set of trips) and discrete (mode)
choice models (which can produce realistic mode shares given the state
of the transport system). POLARIS \cite{AULD2016101} is one example of such a model, where mode choice is one part of the decision process of the simulated agents. They perform mode choices on a trip level and also consider vehicle availability constraints - meaning that a vehicle is only available if it was taken to reach the current location. \cite{Martinez2017carsharing} incorporate a mode choice model within an agent-based model to simulate carsharing operations in the city of Lisbon. Mode choice modelling is performed on a trip level, and vehicle constraints are not considered. Therefore additional calibration of the mode choice parameters was performed. \cite{HEILIG2018151} present a multi-day agent-based simulation that uses a discrete choice model to obtain the modes for each of the agents' trips while considering vehicle constraints. However, they constrain people's choices that if the car was used on a previous trip, it needs to be used on the following, which prevents many of the complex mode decisions that could be made in reality. The TASHA framework makes use of a tour-based mode choice model for the Toronto area \cite{Miller2005}, which distinguishes between trip chains served entirely by the private car mode or by flexibly combinable other modes.

Even though the above mentioned agent-based models that incorporate mode choice models are not covering the entire literature, they represent the current state of modelling in this field. However, there is lack of a thorough analysis of such combinations.

\subsection{MATSim}

For the use cases in this study the agent- and activity-based transport simulation
framework MATSim is used to perform the network simulations. While the authors are affiliated
with the software, the results that are obtained in this work are valid for any
set-up where one has:

\begin{itemize}
\item A discrete choice model with specific choice dimensions (e.g. for a mode choice
model the expected travel times and costs for each mode), which sets up a demand
given the supply.
\item A network simulation model which produces realistic network conditions
given a specific demand, i.e. it is producing the supply characteristics.
\end{itemize}

In the case of MATSim, the network conditions are produced by simulating thousands
of agents (travellers) in the same capacitated network. If too many
people want to use a specific road segment in the network they will be slowed down
because traffic jams emerge. In MATSim this is possible because each link in the
network is simulated as a queue with limited capacity. Therefore even spillback
effects can be simulated using the framework.

On the demand side, MATSim relies on a so-called ``co-evolutionary algorithm''.
Each agent in this framework has a daily plan with several activities with desired start
and end times. Those activities are connected by trips, for which a specific mode
of transport is planned. The selected plans of all agents are iteratively simulated
in the network simulation as described above. After one iteration (usually equal
to one day) all activities and trips that have been performed are translated into
a score. Performing activities at the right time for the right duration gives
positive scores while delays or spending time in traffic give negative scores.
The cumulative score is then a measure for the usefulness of the plan. After each
iteration, a fraction of agents slightly modify their selected plan and add
it back to the set of known plans. Such modifications may be the change of
mode of transport or the route of one or several trips, or the adjustment of the departure
time from an activity. Then, the plan will be simulated in the next iteration,
and a new score is obtained. If an agent does not modify his selected plan, he
chooses between the known ones (which are usually limited to a fixed number) according
to their score. So plans with a high score are more probable to be chosen
and also more probable to be modified and improved. Letting this algorithm run
for a large number of iterations has the following effects: Since all agents perform this
process at the same time, they react to the system state (travel times) that are
produced by all of their plans. Therefore, there is a feedback loop between
supply and demand, i.e. the travel times in the system may fluctuate, but
stabilise once gets close to equilibrium. Ultimately, the only fluctuations in
system state are produced by the random modifications that are applied to a
fraction of plans after each iteration. Figure \ref{fig:scoringmatsim} shows
this process from the perspective of the average agent score over many MATSim
iterations.

\begin{figure}
    \centering
    \includegraphics[width=10cm]{../analysis/plots/scoring.pdf}
    \caption{Schematic visualization of the procession of scores in a typical MATSim run.}
    \label{fig:scoringmatsim}
\end{figure}

The algorithm has one significant advantage: It is very versatile. Basically, as a modeller
one can propose any changes to the plans of the agents and as long as one has
a consistent scoring framework one will arrive at a stochastic user equilibrium.
Those changes can be very simple, e.g. just random changes of transport modes
for randomly selected trips in an agent's plan. A thorough scoring framework would
then penalise plans that are poorly suited (e.g. walking for 50km) or merely
infeasible (e.g. by giving a high penalty to plans where the car is left
behind somewhere in the network without returning home). The two downsides are
that (1) the process takes very long because also pointless realisations of
the plan need to be scored out (e.g. walking for 50km) and (2) each plan need to
be tested at least once in the network simulation. So if around 5\% of the agents
perform replanning after each iteration, there are potentially 5\% of all agents
that perform nonsense alternatives. This has the potential to slow down the
convergence extremely (because it leads to large unnecessary fluctuations in the
network conditions) and forbids a consistent estimation of the network state for
the actual analysis that one wants to perform. To counteract the latter point the replanning ability is turned off for some iterations after the
mean of the average scores in the population has stabilised (i.e. there is
no upward trend anymore). This makes sure that only the available plans
are then selected at random, and hopefully plans with bad scores do not
happen frequently. Furthermore, this ``innovation turnoff'', as depcited in Figure \ref{fig:scoringmatsim}, leads to a jump
in scores, which represents a strong non-linearity. Therefore, any optimisation
algorithm that takes into account the score of early iterations to improve
some scenario parameters (e.g. road pricing) becomes almost useless, because
the step response can go in any direction.

Furthermore, MATSim simulations are notoriously hard to calibrate: Running a simulation
with millions of agents can take minutes to hours (for one iteration), so a whole
run until equilibrium can last days. When trying various values for the scoring
of the travel dimensions, one needs to run a multitude of those simulations, which
is usually not feasible and hence only allows for a rough calibration of the models.

\subsection{Discrete Choice Models}

Discrete choice models are statistical models that assign probabilities to
specific alternatives for a decision task. In a mode choice model, these alternatives
would be the different modes of transport. Each of the alternatives has
some choice dimensions assigned, e.g. the travel time for each mode or the costs.
By using a utility function, similar to the scoring in MATSim, a utility for
each alternative is calculated, for instance:

\begin{equation}\begin{aligned}
V_{\text{car}} &= \alpha_{\text{car}} + \beta_{\text{car}} \cdot t_{\text{car}} + \beta_{\text{cost}} \cdot c_{\text{car}} \\
V_{\text{walk}} &= \alpha_{\text{car}} + \beta_{\text{walk}} \cdot t_{\text{walk}}
\end{aligned}\end{equation}

Here the $t$ would stand for the (estimated / expected) travel time for a
trip, and $c$ would be the cost. The $\beta$s are linear coefficients that translate
all choice dimensions into a generalised utility, and the $\alpha$s are mode-specific
constants that capture the unobserved components in the decision making. Using these
utilities statistical models can be set up. In the case of a simple Multinomial
Logit Model, one would have:

\begin{equation}
\mathbb{P}(\text{car}) = \frac{\exp(V_{\text{car}})}{\exp(V_{\text{car}}) + \exp(V_{\text{walk}})}
\end{equation}

Thus, one obtains a probability of choosing either alternative given the values
for the choice dimensions in the current decision task. Through statistical
inference all model parameters ($\alpha$ and $\beta$) can be estimated from
stated choices in surveys or revealed choices in GPS traces or similar data
sets.

The primary advantage is that those mode choice models can be estimated in
a very short time compared to a MATSim run. It not only allows the modeller
to estimate various parameters but even to try various formulations of the
utility function. These models are backed by thorough theory, they are a standard tool
in transport planning and provide, given the right model formulation has been
found, a solid understanding of the decision behaviour of the population.

The first downside of discrete choice models is that they are usually estimated
on a trip basis. Therefore, all parameters that are available are estimated for
an ``average'' trip during the day of a traveller, regardless of the context
(e.g. the presence of a daily ticket for the public transport).
The second downside of discrete choice models is that they do not take into account
a dynamic demand situation: One needs to assume a specific travel time (but can
do sensitivity analyses). However, given a large set of travellers who are confronted
with similar decisions the utilisation of the infrastructure may change the
travel times and therefore the likelihood of an agent to choose a specific mode.
Being able to insert the equilibrium travel characteristics into the equations
would produce the ``actual'' expected choice probabilities for a use case, not
only the ones under fixed assumptions about these values.

Therefore, the study at hand tries to establish a loop between a detailed dynamic network
simulation (in this case MATSim) and a discrete mode choice model. However, this
integration is not as straightforward as it might seem. In the following chapter,
the authors propose one approach of integration and outline the experienced problems
and difficulties while doing so.
