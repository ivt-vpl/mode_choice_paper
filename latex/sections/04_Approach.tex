\section{Approach}

Given the advantages and disadvantages of MATSim and discrete choice models alike
the task of the study at hand is twofold:

\begin{enumerate}
    \item To set up a framework that makes it easy to integrate mode choice models into MATSim
    \item To show the results that we obtain by using an initially trip-based model for the whole plan-based choice process
\end{enumerate}

The basic structure of the framework that has been set up can be seen in Figure
\ref{fig:mode_choice_flowchart}. On the right side is the simulator, in this case
MATSim. There is an agent plan that goes through the choice process, such that a new
updated plan is generated at the and. In the updated plan the modes of all trips may have changed.
Afterwards, all plans are sent to the simulator to perform the next iteration. After the
iteration has finished, the new travel times, service costs, congestion levels
 are ready to be consumed by the choice model.

\subsection{Choice Model Components}

The different stages of the mode choice model itself (middle part in Figure \ref{fig:mode_choice_flowchart})
are described in the next sections. The model exists in two
different versions: A trip-based model and a tour-based model. The former one
sequentially assigns new modes to the trips of an agent's plan (and is therefore
close to the original formulation of trip-based mode choice models), whereas the
latter one considers whole chains of modes to choose from.

\begin{figure}
    \includegraphics[width=\textwidth]{figures/ModeChoiceFlowChart.pdf}
    \caption{Schematic visualization of the mode choice framework.}
    \label{fig:mode_choice_flowchart}
\end{figure}

\subsubsection{Alternative Generator}

For each choice situation, multiple alternatives are generated. In the trip-based
case the choice situations are the single trips of a plan, while for a tour-based
model these choice situations are tours, i.e. sequences of trips. The two concepts
are visualized in Figure \ref{fig:choice_process}: In the trip-based model, each mode selection for the plan would
be performed only based on the utility of the modal alternatives for the current trip.
The set of alternatives is always the set of feasible modes for the agent.

On the other hand, the plan in Figure \ref{fig:choice_process} contains two home-based
tours, one that consists of the first four activities and one that is comprised
by the last three ones. Contrary to the trip-based version, where five independent
choices are to be made, here only two choices are performed. The choice
set in those two stages is much larger because it consists of all possible
permutations of modes that are available for the respective tour.

In either case, the modes that are available are determined first for each agent
through the \textit{Mode Availability} component. It merely acts as an early
filtering of modes to avoid the processing of irrelevant modes for the sake of
computational speed. An example for such a filtering would be the removal of the
\textit{car} mode from the set of feasible modes because the agent is under age
or does not have a driving license.

To determine how a tour is defined there is a \textit{Tour Finder}. The standard
implementation examines the locations of the activities to cut the plans into
home-based tours, but any more complex approach is possible. Note that a pure
``plan-based'' model falls into this framework. If a tour is defined as starting
at the first activity and ending at the last activity of a plan there will be
only one choice situation with a choice set consisting of all feasible realisations
of the agent's plans.

\begin{figure}
    \includegraphics[width=\textwidth]{figures/ChoiceProcess2.pdf}
    \caption{Trip-based vs. tour-based choice process}
    \label{fig:choice_process}
\end{figure}

In a second stage, the choice situations are further filtered if they
violate one of the given \textit{constraints}. Those constraints have knowledge
about the plan itself (for instance, it is known at which locations all the activities
take place), and they are informed about the outcomes of the preceeding choice
situations. For instance, in Figure \ref{fig:choice_process} the modes for the 3rd trip
would be filtered \textit{after} the choices for the first two trips have been
performed. Hence, the constraints can adapt to previously taken decisions. Likewise,
the second \textit{tour} decision would have knowledge about the first one.

%A simple constraint for a trip-based model could be: ``Remove \textit{car} from
%the choice set if the current trip does not start at the location where the last
%trip with the mode \textit{car} was ending.'' This way one would make sure that
%the car cannot be used at a location where it has not been moved to before. A more
%detailed look at such ``vehicle continuity constraints'' will be given further
%below.

The first filtering of choices does not have knowledge about the trip characteristics
themselves. For instance, if the feasibility of \textit{public trasport} is considered at this
stage it cannot be filtered out because there is no service at the requrested time of day.
However, the framework allows for such constraints:
There is a second filtering stage after all the trip and tour characteristics have
been estimated, as is described in the next section.

\subsubsection{Estimation of choice dimensions}

The second main stage in the choice process is the estimation of the trip
characteristics $\chi_i$. Usually, it means that for each alternative $i$ a complex
routing has to be performed. Some examples of the resulting characteristics are:

\begin{itemize}
    \item The travel time and costs by \textit{car}.
    \item The waiting time, the in-vehicle time and the number of interachages for \textit{public transport}.
    \item The travel time and difference in altitude for \textit{walk} and \textit{bike}
    \item The cumulative travel costs for all \textit{public transport} trips on a tour, considering
    the availability of short period tickets, single ride tickets or daily passes
\end{itemize}

Note that those routings are a computationally heavy task, but that in most cases
a lot of caching can be performed (i.e. in most cases it is irrelevant in which tour
alternative a trip is embedded to obtain the travel time). After the
estimation of the trip characteristics a second filtering stage is
performed as described above. Here all the information from the routing can be
used.

What remains is a set of feasible alternatives for a choice situation with
known values for the explanatory variables. Based on that data, a utility $\hat u_i$ is
computed for each alternative $i$. For that purpose any formulation of the utility
function can be used. However, the idea here is to use one that has been estimated
before by means of discrete choice modelling.

\subsubsection{Candidate selection}

Finally, given the \textit{estimated} utilities $\hat u_i$, one
of the alternatives is chosen in each choice situation. Currently, two approaches are implemented:

\begin{itemize}
    \item \textbf{Multinomial selection} considers the estimated utilities of all
    alternatives, and samples one of them according to the probability
    $\mathbb{P}(i) = \frac{\exp(\hat u_i)}{\sum_{i'} \exp(\hat u_{i'})}$.
    \item \textbf{Best-response selection} selects the alternative with the highest estimated utility.
\end{itemize}

In this paper, only the first approach is considered because conceptually it has
a higher comparability to the original discrete choice models. The main property
of the approach is that decisions are stochastic: If two alternatives have roughly
the same utility, they appear almost equally often in subsequent replanning attempts
of the agent.

The best-response approach, on the other hand, makes sure that if trip characteristics
have converged, \textit{always} the same option is chosen. It remains to be explored
if this may be beneficial for the simulation dynamics in MATSim or other simulators.

After the selection, the choices made are incorporated in the agent's plan.

\subsection{Integration with MATSim}

Currently, the mode choice model is integrated into MATSim in a way that only
the network simulation of the framework is used. Most of the other parts are
disabled. For instance, the agents in the test setup do not collect experienced
plans and select the best among them, rather they only hold one plan at a time.
Furthermore, other replanning strategies, like random modifications to the depature
times are not taking place. The only choices that are made in the test cases are mode
decisions and the implicit route choices (which follow a best-response approach).
Tests on integrating the mode choice model into the whole MATSim environment are ongoing
and pathways will be discussed further below.

Nevertheless, some predictions can already be made: Using the framework
at hand convergence should be much quicker, because no irrational mode choice
decisions are done at random. Likewise, convergence on the supply side  should effected positively
because without irrational trips taking place the travel characteristics
of the system can stabilise more easily.

While the better convergence behaviour is desirable, it also introduces a new
layer of complexity to the MATSim approach. Because the choice model relies heavily
on reasonable values for the explanatory variables, it is crucial to get these estimates
right. Therefore, it is important to verify that close to the equilibrium state
the estimated / expected travel times and other dimensions converge towards those
that are actually observable in the simulation. Only then is it possible to claim
that the mode choice model produces consistent unbiased decisions.
