\section{Case Study}

In the following, a case study for a simulation scenario of the city of Zurich
shall be presented. The synthesis of
the agent population, the network and all the locations in the MATSim scenario is described
in \cite{ivtbaseline}. Due to the complexity of the model with various activity
types and four modes of transport (\textit{car}, \textit{public transport}, \textit{bike},
and \textit{walking}) it used to be difficult to properly calibrate the model. In
the study at hand a discrete mode choice model is tested against the scenario to
check how well different configurations replicate the reference data. For comparison
the modal split by distance class is used. The reference data has been obtained from
the Swiss Microcensus on Transport and Mobility for 2015 \cite{mz15}.

\subsection{Model Definition}

To set up the mode choice model one needs to define utility
functions for different modes. A mode choice model for the city of Zurich
is available from an ongoing project (Table \ref{tab:scoring}). Based on this model the utility functions for
the four modes are defined as follows:

\begin{equation}\begin{aligned}
u_{\text{car}}(\chi) &=
\alpha_{\text{car}} \\
&+ \beta_{\text{travelTime,car}} \cdot \chi_{\text{travelTime,car}} \\
&+ \beta_{\text{travelTime,car}} \cdot \theta_{\text{parkingSeachPenalty}} \\
&+ \beta_{\text{travelTime,walk}} \cdot \theta_{\text{accessEgressWalkTime}} \\
&+ \beta_{\text{cost}} \cdot \left(\frac{\chi_{\text{crowflyDistance}}}{\theta_{\text{averageDistance}}}\right)^\lambda \cdot \chi_{\text{cost,car}}
\end{aligned}\end{equation}

\begin{equation}\begin{aligned}
u_{\text{pt}}(\chi) &=
\alpha_{\text{pt}} \\
&+ \beta_{\text{numberOfTransfers}} \cdot \chi_{\text{numberOfTransfers}} \\
&+ \beta_{\text{inVehicleTime}} \cdot \chi_{\text{inVehicleTime}} \\
&+ \beta_{\text{transferTime}} \cdot \chi_{\text{transferTime}} \\
&+ \beta_{\text{accessEgressTime}} \cdot \chi_{\text{accessEgressTime}} \\
&+ \beta_{\text{cost}} \cdot \left(\frac{\chi_{\text{crowflyDistance}}}{\theta_{\text{averageDistance}}}\right)^\lambda \cdot \chi_{\text{cost,pt}}
\end{aligned}\end{equation}

\begin{equation}
u_{\text{bike}}(\chi) = \alpha_{\text{bike}}
+ \beta_{\text{travelTime,bike}} \cdot \chi_{\text{travelTime,bike}}
+ \beta_{\text{age,bike}} \cdot \text{max}\left( 0\ , a_{\text{age}} - 18 \right)
\end{equation}

\begin{equation}
u_{\text{walk}}(\chi) = \alpha_{\text{walk}}
+ \beta_{\text{travelTime,walk}} \cdot \chi_{\text{travelTime,walk}}
\end{equation}

While the $\alpha$, $\beta$ and $\lambda$ variables stem from the choice model, the
$\theta$ represent calibration parameters that needed to be included to
allow for a fair competition of the four modes, or constants that are used in the
original mode choice model formulation. The $a$ paramters are agent-specific
socio-demographic attributes. All $\chi$ are explanatory variables that need to be estimated
from the simulation.

The costs $\chi_{cost,car}$ and $\chi_{cost,pt}$ are backed by rather detailed
cost calculations, which are omitted here for brevity. The values used in the study
at hand are summarized in Table \ref{tab:scoring}.
%\begin{table}
%	\centering
%	% space before first and after last column: 1.5pc
%	% space between columns: 3.0pc (twice the above)
%	% \setlength{\tabcolsep}{1.5pc}
%	% -----------------------------------------------------
%	% adapted from TeX book, p. 241
%	% \newlength{\digitwidth} \settowidth{\digitwidth}{\rm 0}
%	\catcode`?=\active \def?{\kern\digitwidth}
%	% -----------------------------------------------------
%	\caption{Parameters of the discrete mode choice model \cite{Felix2018}.}
%	\label{tab:MNLTable}
%	\begin{tabular}[c]{ l | r r r }
%		\hline
%		Mode & Variable & Parameter & robust-t test\\
%		\hline
%		Walking & Constant& 0.630 & 0.32\\
%		& Travel time [min] & -0.141 & -2.30\\
%		\hline
%		Cycling & Constant & -0.100 & 0.46\\
%		& Travel time [min]& -0.080 & -3.94\\
%		&Age ($>$ 17) & -0.049 & -2.85 \\
%		\hline
%		Car & Constant & 0.827 & 2.00 \\
%		& Travel time [min]& -0.067 & -7.61\\
%		\hline
%		Public Transport
%		& Travel time [min]& -0.019 & -3.33\\
%		& Access/egress time [min]&-0.080 & -3.23\\
%		&Waiting time [min] & -0.038 & -1.50\\
%		&Number of transfers & -0.170 & -1.10\\
%		%	\hline
%		%	FFCS & Constant & 0.0 & na \\
%		%	& Travel time [min] & -0.0667 & na \\
%		%	& Access time [min] & -0.0804 & na \\
%		\hline
%		Cost & [CHF]& -0.126&-10.60\\
%		\hline
%		$\lambda_{distance cost}$ & & -0.400 & -6.74 \\
%		\hline
%		Number of decision makers:&&&317 \\
%		Number of observations: &&&1875	\\
%%%		Rho2:  &&&		0.360 \\
%		Estimated parameters: &&& 20	\\
%		\hline
%	\end{tabular}
%\end{table}
\begin{table}
    \centering
    \begin{tabular}{l|lrl}
        \hline\hline
        Car & $\alpha_{\text{car}}$ & $0.827$ & $ $ \\
        & $\beta_{\text{travelTime,car}}$ & $-0.0667$ & $[\text{min}^{-1}]$ \\
        \hline
        Public Transport & $\alpha_{\text{pt}}$ & $0.0$ & $ $ \\
        & $\beta_{\text{numberOfTransfers}}$ & $-0.17$ & $ $ \\
        & $\beta_{\text{inVehicleTime}}$ & $-0.0192$ & $[\text{min}^{-1}]$ \\
        & $\beta_{\text{transferTime}}$ & $-0.0384$ & $[\text{min}^{-1}]$ \\
        & $\beta_{\text{accessEgressTime}}$ & $-0.0804$ & $[\text{min}^{-1}]$ \\
        \hline
        Bike & $\alpha_{\text{bike}}$ & $-0.1$ & $ $ \\
        & $\beta_{\text{travelTime,bike}}$ & $-0.0805$ & $[\text{min}^{-1}]$ \\
        & $\beta_{\text{age,bike}}$ & $-0.0496$ & $[a]$ \\
        \hline
        Walking & $\alpha_{\text{walk}}$ & $0.63$ & $ $ \\
        & $\beta_{\text{travelTime,walk}}$ & $-0.141$ & $[\text{min}^{-1}]$ \\
        \hline
        Others & $\beta_{\text{cost}}$ & $-0.126$ & $[\text{CHF}^{-1}]$ \\
        & $\lambda$ & $-0.4$ & $ $ \\
        & $\theta_{\text{averageCrowflyDistance}}$ & $40$ & $[\text{km}]$ \\
        \hline
        Calibration & $\theta_{\text{parkingSearchPenalty}}$ & $6$ & $[\text{min}]$ \\
        & $\theta_{\text{accessEgressWalkTime}}$ & $5$ & $[\text{min}]$ \\
        \hline\hline
    \end{tabular}
    \caption{Parameters of the discrete mode choice model}
    \label{tab:scoring}
\end{table}

For the tour-based experiments home-based tours are chosen, i.e. all trips
on a travel sequence from and to home are jointly assigned a utility. To do so
the \textit{estimated} utilities $\hat u_j$ of the single trips $j$ on tour alternative $i$ are summed up:

\begin{equation}
\hat u_i = \sum_j \hat u_{i,j}
\end{equation}

Note that this ``total utility approach'' is not an obvious choice. In fact, it
is one formulation among many, some of which have been examined in more detail
in \cite{horl_abmtrans18}.

\subsection{Constraints}

In order to allow for a realistic travel behaviour vehicle constraints need
to be introduced. They make sure that a vehicle can only be used where it has been moved to
before. Furthermore, it should be made sure that the vehicle arrives back home
after each day, here we require that it does so after each tour.

For the tour-based model, such a constraint is quite easy to establish. Because
a tour is considered it is already clear that the start- and endpoint of the
sequence under consideration is at the same spot. Therefore, it remains to
check the following constraint on the trips included in a tour:

\begin{algorithm}
    \begin{algorithmic}
        \State \textbf{Initialize:} $vehicleLocation \gets$ Agent's home location
        \State \textbf{For each} trip with mode \textit{car} \textbf{do}:
        \State \ \ \ \ \ \ \textbf{If} the origin of the current trip is \textbf{not} $vehicleLocation$ \textbf{Then}
        \State \ \ \ \ \ \ \ \ \ \ \ \ \textbf{Return} Constraint is violated
        \State \ \ \ \ \ \ \textbf{End If}
        \State \ \ \ \ \ \ $vehicleLocation \gets$ destination of current trip
        \State \textbf{End For}
        \State \textbf{If} $vehicleLocation$ is \textbf{not} agent's home location \textbf{Then}
        \State \ \ \ \ \ \ \textbf{Return} Constraint is violated
        \State \textbf{End If}
    \end{algorithmic}
    \caption{Tour-based vehicle continuity constraint}
    \label{alg:tour_based}
\end{algorithm}

Note that in this constraint one never needs to know about the previous element, i.e.
the tour that comes before the one in question. For a trip-based model this is
necessary. A very simple version would be only one check:

\begin{algorithm}
    \begin{algorithmic}
        \State \textbf{If} \textit{car} has been used in a preceeding trip \textbf{Then}
        \State \ \ \ \ \ \ $vehicleLocation \gets$ destination of the last preceeding trip with mode \textit{car}
        \State \textbf{Else}
        \State \ \ \ \ \ \ $vehicleLocation \gets$ Agent's home location
        \State \textbf{End If}
        \State \textbf{If} \textit{car} shall be used in current trip \textbf{Then}
        \State \ \ \ \ \ \ \textbf{If} origin of current trip is \textbf{not} $vehicleLocation$ \textbf{Then}
        \State \ \ \ \ \ \ \ \ \ \ \ \ \textbf{Return} Constraint is violated
        \State \ \ \ \ \ \ \textbf{End If}
        \State \textbf{End If}
    \end{algorithmic}
    \caption{Simple trip-based vehicle continuity constraint}
    \label{alg:simple_trip_based}
\end{algorithm}

Here, one makes sure that whatever happened in the trips before
is consistent with the agent now attempting to use
his car at the current location. However, this doesn't make sure that the car
arrives back at home. In fact, to achieve this, the constraint becomes

\begin{algorithm}
    \begin{algorithmic}
        \State \textbf{If} \textit{car} shall be used for the current trip \textbf{Then}
        \State \ \ \ \ \ \ \textbf{Execute} Algorithm \ref{alg:simple_trip_based}
        \State \textbf{Else}
        \State \ \ \ \ \ \ Obtain $vehicleLocation$ as in \ref{alg:simple_trip_based}
        \State \ \ \ \ \ \ \textbf{If all of}
        \State \ \ \ \ \ \ \ \ \ \ \ \ $vehicleLocation$ is at current origin
        \State \ \ \ \ \ \ \ \ \ \ \ \ $vehicleLocation$ is \textbf{not} at agent's home location
        \State \ \ \ \ \ \ \ \ \ \ \ \ current origin is \textbf{not} visited again later in the tour
        \State \ \ \ \ \ \ \textbf{Then}
        \State \ \ \ \ \ \ \ \ \ \ \ \ \textbf{If} any mode except \textit{car} shall be used for current trip \textbf{Then}
        \State \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{Return} Constraint is violated
        \State \ \ \ \ \ \ \ \ \ \ \ \ \textbf{End If}
        \State \ \ \ \ \ \ \textbf{End If}
        \State \textbf{End If}
    \end{algorithmic}
    \caption{Advanced trip-based vehicle continuity constraint}
    \label{alg:advanced_trip_based}
\end{algorithm}

The first part of this advanced trip constraint is exactly as in the simple one:
To make sure that the vehicle is present if one wants to depart with it. The second
part makes sure that the vehicle returns home: If an agent shall perform a trip from
a location (maybe walking), but by doing so it is clear that there is no chance
of returning the car home, the agent \textit{must} use his car for this trip,
i.e. all other modes are forbidden.

So where is the difference between the advanced trip constraint and the tour
constraint? It lies in the order of decisions. In the trip-based model all choices
are made step by step. So it may make sense for the first trip itself to be performed
walking because it is just a short trip to a shop. However, once this decision
is made all following decisions are dependent on it. So after the shop the agent
may go to work, which is 50km away. He is now bound to use public transport
although the connection might be rather bad. In the tour-based model all decisions
in one tour are made jointly. Because of the large negative utility that a public
transport commute would bring the score of the whole tour would be dragged down
and hence it would make sense to use a car for the long trip. However, to comply
with the vehicle constraint, the car would also be used for the short first trip.
In general, making choices on the tour-level should produce much more reasonable
decisions than making choices for each trip.

\subsection{Results}

The following sections are used to
show the differences between the tour-based and trip-based model, as well as
the differences between the two trip constraints.

\subsubsection{Mode shares}

First of all it is interesting to explore how well the models reproduce the mode shares
that can be observed in reality.
Figure \ref{fig:mode_share_comparison} shows the comparison between the reference
and two realizations of the mode choice model: On the left side the tour-based model
is used while on the right side the trip-based one is shown. The first observation
is that the tour-based model approximates the reference quite well while the
trip-based model with the advanced trip constraint has a larger error. Especially
the \textit{car} mode is under-represented, which can be explained by the reasoning in the
previous chapter. Since the first trip has a large weight in the decision process,
agents for which it makes sense to use their car later during the day but have decided
against it on the first trip are forced to use other modes. The same is true for
agents who chose to use their bike for the first trip.

\begin{figure}
    \includegraphics[width=\textwidth]{../analysis/plots/comparison_plan_vs_trip.pdf}
    \caption{Comparison of mode shares by distance class for the plan/tour-based model and
    the trip-bases model. The reference points are the modal shares at twenty quantiles of
    the reference distance distribution (vertical lines).}
    \label{fig:mode_share_comparison}
\end{figure}

\subsubsection{Trip-based constraints}

The second comparison is between the two trip-based constraints. Figure
\ref{fig:trip_daytime_comparison} shows the simple one on the left side and
the advanced one on the right side. In this case the mode share by time of day
is shown. Clearly, the simple constraint does not consider
whether a vehicle arrives at home at the end of the day. Due to the
trip-by-trip decision making cars become less and less used over the course
of the day and most of the vehicles are left behind somewhere in the city. The advanced
constraint improves the situation. Despite the agents still using the car
less than they are supposed to the constraint forces them to return home at least. Therefore,
more \textit{car} trips can be seen in the evening hours.

\begin{figure}
    \includegraphics[width=\textwidth]{../analysis/plots/comparison_simple_vs_advanced.pdf}
    \caption{Comparison of the simple vehicle continuity constraint and the advanced one for the trip-based model}
    \label{fig:trip_daytime_comparison}
\end{figure}

\subsubsection{Runtime}

Finally, the runtime of the different approaches is compared in Figure \ref{fig:runtime_comparison}.
On the left side the constrained models can be seen whereas on the right side the
constraints have been deactivated (which means that the model produces inconsistent plans).

For the constrained models, the tour/plan-based model takes
longer to execute. This is due to the higher effort in generating the more complex
chains rather than simply providing a set of feasible modes.
Furthermore, the advanced trip constraint takes more time to run than the
simple one. This is due to the expensive checking whether a specific mode should
be enforced.

Interestingly, the constrained models are
quicker in comparison to the unconstrained ones. This may come as a surprise, but is easy to explain. The
unconstrained case considers every possible alternative whereas the constrained models
filter out most of them on the fly. As described above, most of these constraints
already hit before any (computationally expensive) routing is performed.

To put the runtime in perspective it should be noted that the scenario uses a
sample of the agent population of around 200k agents. In each iteration around
22k of them perform mode choice. The simulations are performed on a slightly
outdated Intel Xeon E7-4870 machine from 2011 on 80 cores.
It should be noted that since the decisions are made independently for all agents, the choice
process can be parallelised up to the hardware limits. The obtained runtimes
and prospects for scalability promise a good performance for a 100\% sample or even a whole
Switzerland scenario.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../analysis/plots/runtime.pdf}
    \caption{Runtimes for the mode choice models, compared with the unconstrained case.}
    \label{fig:runtime_comparison}
\end{figure}

\subsection{Estimation quality}

While the fit with the reference data may
seem good for the tour-based model, one needs to be careful that the choice model is operating on the
right data. Here, this specifically means that one should make sure that
the estimated travel times that are fed into the choice model do actually
predict the travel times that are observed in the simulation.

For that purpose the prediction error is tracked: Whenever an agent makes
the choice to use \textit{car} for a trip, the estimated travel time, provided by
MATSim, is saved. Right after, the plan is simulated in MATSim and
the travel time is measured \textit{during} the simulation. Hence, a pair of
predicted and observed travel times is available for all of those trips.
Figure \ref{fig:prediction} shows the average relative prediction error. Positive values
mean that the travel time is overestimated while negative values mean that
the predicted travel time is too low.

The median prediction error at 0\% shows that in general travel times are predicted
quite well except in the first iterations when the system is not yet close to the
stochastic user equilibrium. However, one can also see that the average error is
at -2.5\% which means that the travel times are slightly underestimated. There
are multiple reasons why this can happen. For instance, the travel times on each link
in the network are averaged over five minute intervals by default in MATSim. This can
lead to uncertainties and may be adjusted in future experiments.

While the error in the presented use case seems acceptably low and it can be
assumed that the produced mode choices are almost unbiased, it is an example
that shows how important it is to verify the prediction quality when executing a
discrete choice model in the loop.

\begin{figure}
    \includegraphics[width=\textwidth]{../analysis/plots/prediction.pdf}
    \caption{Prediction error of the tour-based mode choice model}
    \label{fig:prediction}
\end{figure}
